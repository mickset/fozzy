$(document).ready(function(){
	clearInputs();
});

function clearInputs(){
	$('input.placeholderacc, textarea.placeholderacc').each(function(){
		var _el = $(this);
		var _val = _el.val();
		
		_el.bind('focus', function(){
			if (this.value == _val) {
				this.value = '';
				$(this).removeClass('placeholderacc');
			}
			$(this).addClass('input-focus');
		}).bind('blur', function(){
			if(this.value == '') {
				this.value = _val;
				$(this).addClass('placeholderacc');
			}
			$(this).removeClass('input-focus');
		});
	});
}