$(document).ready(function(){
	initFooter();
	initHeader();
	clearInputs();
	clearInputs2();
	jQuery.customForm({
		select: {
			elements: 'select.customSelect',
			structure: '<div class="selectArea"><div class="selectIn"><div class="selectText"></div></div></div>',
			text: '.selectText',
			btn: '.selectIn',
			optStructure: '<div class="selectSub"><ul></ul></div>'
		}
	});
	$('.gall').gallery({
		listOfSlides: 'ul > li',
		effect: true,
		duration: 500,
		autoRotation: 5000
	});
	$('.slider').gallery({
		listOfSlides: 'div.hold > ul > li',
		duration: 500,
		autoRotation: 5000
	});
	$('a.simplebox').simplebox({
		overlay:{opacity:0.69}
	});
	$('.simplebox-new').simplebox({
		top: 0,
		duration: jQuery.browser.platform == 'android' ? 0 : 300,
		overlay:{opacity:0.69}
	});
	initFilters();
	initSearch();
	initAdvanced();
	actionBlock();
	if(typeof $.fn.datepicker == 'function'){
		$( "#datepicker, #datepicker2" ).datepicker({
			showOtherMonths: true,
      		selectOtherMonths: true,
            dateFormat: "yy-mm-dd"
		}).on('change', function () {
            var selected = $(this).val();
            var div = $(this).parents('.action-block');
            $(div).find('div.toggle').html(selected);
//            $('#selBulkFilterCreationDate').val(selected);
            $('#'+$(this).data("id")).val(selected);
        });
	}
	sideHold();
	animateInfo();
	openDomens();
	domainsInfo();
	initPopups();
	expandedCode();
	hostrecords();
    if (!$.browser.mobile) {
		if(!($.browser.msie && $.browser.version < 9)) {
			if(typeof $.fn.parallax == 'function') $('.parallax').parallax();
		}
	}
    if(typeof $.fn.gradientText == 'function') {
    	$('.hosting-info > .bg > .text > .color').gradientText({
	        colors: ['#ef7500', '#ffc000']
	    });
    }
	$('.tabs').each(function(){
		$(this).tabs();
	});
});

function initHeader () {
	$('header.header').each(function(){
		var hold = $(this);
		var lineTop = hold.position().top;

		var _scroll = function () {
			if($(window).scrollTop() > lineTop){
				$('body').addClass('fixed-header');
			}
			else{
				$('body').removeClass('fixed-header');
			}
		}
		_scroll();
		$(window).bind('scroll', _scroll);
	});
}

function hostrecords(){
	$('.hostrecords').each(function(){
		var hold = $(this);
		var clone = hold.find('div.row:eq(0)').clone();

		clone.find('.add, .remove').remove();
		clone.find('.detected').removeClass('detected');
		clone.prepend('<div class="add" />');
		
		hold.on('click', '.add', function(){
			hold.find('div.row:last').after(clone.clone());
			hold.find('div.row:not(:last):has(.add)').prepend('<div class="remove" />').find('.add').remove();
			initPopups();
		});
		hold.on('click', '.remove', function(){
			$(this).parent().remove();
		});
	});
}

function expandedCode(){
	$('.expanded-wrap').each(function(){
		var hold = $(this);
		var link = hold.find('.expanded-code');
		var edit = hold.find('.btn-edit');
		var text = link.text();
		
		edit.click(function(){
			link.empty().append('<input type="text" value="'+text+'" />').find('input').focus();
			return false;
		});
	});
}

function initPopups(){
	$('tr .action-block, .opt .action-block, .hostrecords .action-block, #divFolderList .action-block, #ulFilterListContainer .action-block, .modal2 .action-block, #frmFindDomain .action-block')
	.not('.detected').addClass('detected').each(function(){
		var hold = $(this);
		var link = hold.find('.toggle');
		var box = hold.find('.pop');
		
		box.css({display: 'none'});

		link.click(function(){
			if(!hold.hasClass('open')){
				hold.addClass('open');
				box.css({display: 'none'}).slideDown(300, function(){
					box.css({display: 'block'});
				});
			}
			else{
				box.slideUp(300, function(){
					hold.removeClass('open');
				});
			}
			return false;
		});
		
		$(document).bind('click touchstart mousedown', function(e){
			if(!($(e.target).parents().index(box) != -1 || $(e.target).index(box) != -1) && !$(e.target).hasClass('ui-datepicker-prev') && !$(e.target).hasClass('ui-datepicker-next')){
				box.slideUp(300, function(){
					hold.removeClass('open');
				});
			}
		});
	});
}

function domainsInfo(){
    $('.domains-info:not(#divDomainFackeHeader)').each(function(){
		var hold = $(this);
		var all = hold.find('tr th input:checkbox');
		var link = hold.find('tr.top input:checkbox');
		var box = hold.find('.selected-info');
        var fakeBox = $('#divDomainFackeHeader .selected-info');
		var text = hold.find('.selected-info > span');
        var fakeText = $('#divDomainFackeHeader .selected-info > span');

        var transferBox = hold.find('.transfer-box');
        if (transferBox.length) {
            return;
        }

		var _change = function(){
			if(link.filter(':checked').length > 0){
				if(link.filter(':checked').length == 1){
					text.text('1 DOMAIN');
                    fakeText.text('1 DOMAIN');
				} else {
					text.text(link.filter(':checked').length + ' DOMAINS');
                    fakeText.text(link.filter(':checked').length + ' DOMAINS');
				}
                var flag = false;
                link.filter(':checked').each(function() {
                    if ($(this).data('status') == 1) {
                        flag = true;
                    }
                    if (!domainIsAllowed(DomainOperations.PushToAnotherAccount, $(this).data('permissions'))) $('.lockerPush').css({'display' : 'none'}).addClass('hideLink');
                    if (!domainIsAllowed(DomainOperations.Renew, $(this).data('permissions'))) $('.lockerRenew').css({'display' : 'none'}).addClass('hideLink');
                    if (!domainIsAllowed(DomainOperations.SetNameServers, $(this).data('permissions'))) $('.lockerNameservers').css({'display' : 'none'}).addClass('hideLink');
                    if (!domainIsAllowed(DomainOperations.SetHostRecords, $(this).data('permissions'))) $('.lockerHostRecords').css({'display' : 'none'}).addClass('hideLink');
                    if (!domainIsAllowed(DomainOperations.SetForwarding, $(this).data('permissions'))) $('.lockerForward').css({'display' : 'none'}).addClass('hideLink');
                    if (!domainIsAllowed(DomainOperations.SetRenew, $(this).data('permissions'))) $('.lockerSetRenew').css({'display' : 'none'}).addClass('hideLink');
                    if (!domainIsAllowed(DomainOperations.SetLockStatus, $(this).data('permissions'))) $('.lockerLocking').css({'display' : 'none'}).addClass('hideLink');
                    if (!domainIsAllowed(DomainOperations.SetWhois, $(this).data('permissions'))) $('.lockerWhoisPrivacy').css({'display' : 'none'}).addClass('hideLink');
                    if (flag) {
                        box.find('.locker').each(function() {
                            $(this).css({'display' : 'none'});
                        });
                        fakeBox.find('.locker').each(function() {
                            $(this).css({'display' : 'none'});
                        });
                    } else {
                        box.find('.locker').each(function() {
                            if (!$(this).hasClass('hideLink'))
                                $(this).css({'display' : 'block'});
                        });
                        fakeBox.find('.locker').each(function() {
                            if (!$(this).hasClass('hideLink'))
                                $(this).css({'display' : 'block'});
                        });
                    }
                });
				box.css({display: 'block'});
                fakeBox.css({display: 'block'});

                $('#divDomainFackeHeader').addClass('remoteHeader');
			} else {
				box.css({display: 'none'});
                fakeBox.css({display: 'none'});
                $('#divDomainFackeHeader').removeClass('remoteHeader');
			}

            $( window ).trigger('scroll');
		}

		_change();
		link.bind('change', _change);

		all.bind('change', function(){

            if($(this).is(':checked')) {
                link.prop('checked', true).customForm('refresh');
            }else {
                link.prop('checked', false).customForm('refresh');
            }

            var fakeCheckbox = $('.domains-info.fakeHeader tr th input:checkbox');
            if(fakeCheckbox.length > 0) {
                $(fakeCheckbox).prop('checked', $(this).is(':checked'));
            }

            _change();
            $('#tableDomainListBody tr.container').draggable( "option", "disabled", $(link).is('checked') );
		});

		link.bind('change', function(){
			if(link.length == link.filter(':checked').length){
				all.prop('checked', true).customForm('refresh');
			}
			if(link.length == link.not(':checked').length){
				all.prop('checked', false).customForm('refresh');
			}
		});
	});
}

function openDomens(){
	$('tr.top:has(.name)').each(function(){
		var hold = $(this);
		var link = hold.find('.name .toggle-open');
		var box = hold.next('tr.in');
		
		link.click(function(){
			if(!hold.hasClass('open')){
				hold.addClass('open');
				box.addClass('open');
				box.find('div.hold').css({display: 'none'}).slideDown(300);
                hold.find('input.domainData').prop('checked', true).trigger('change');
			}
			else{
				box.find('div.hold').slideUp(300, function(){
					hold.removeClass('open');
					box.removeClass('open');
                    hold.find('input.domainData').prop('checked', false).trigger('change');
				});
			}
			return false;
		});
	});
}

function animateInfo(){
	$('.notification').each(function(){
		var hold = $(this);
		var link = hold.find('.close');
		
		link.click(function(){
			hold.slideUp(300);
			return false;
		});
	});
}

function sideHold(){
	$('.side-hold').each(function(){
		var hold = $(this);
		var link = hold.find('.toggle-folders');
		var close = hold.find('.toggle-close');
		var folders = hold.find('.folders');
		var side = hold.find('.side-bg');
		
		link.click(function(){
			hold.addClass('open');
			hold.css({marginLeft: 0}).animate({marginLeft: 225}, {queue:false, duration: 300});
			folders.css({overflow: 'hidden', width: 0, marginLeft: 0}).animate({width: 225, marginLeft: -225}, 300, function(){
				folders.css({overflow: 'visible'});
			});
			side.css({width: 0, left: 0}).animate({width: 225, left: -225}, {queue:false, duration: 300});
			link.css({marginLeft: 0}).animate({marginLeft: -225}, {queue:false, duration: 300});
			return false;
		});
		close.click(function(){
			hold.removeClass('open');
			folders.css({overflow: 'hidden', width: 0, marginLeft: 0});
			side.css({width: 225, left: -225}).animate({width: 0, left: 0}, {queue:false, duration: 300});
			hold.css({marginLeft: 225}).animate({marginLeft: 0}, {queue:false, duration: 300});
			link.css({marginLeft: -225}).animate({marginLeft: 0}, {queue:false, duration: 300});
			return false;
		});
	});
}

function actionBlock(){
	$('.fl2 .action-block').each(function(){
		var hold = $(this);
		var link = hold.find('.toggle-hold .toggle');
		var box = hold.find('> .pop');

		box.css({display: 'none'});
		
		link.click(function(){
			if(!hold.hasClass('open')){
				hold.addClass('open');
				box.css({display: 'none'}).slideDown(300, function(){
					box.css({display: 'block'});
				});
			}
			else{
				box.slideUp(300, function(){
					hold.removeClass('open');
				});
			}
			return false;
		});
		
		$(document).bind('click touchstart mousedown', function(e){
			if(!($(e.target).parents().index(box) != -1 || $(e.target).index(box) != -1) && !$(e.target).hasClass('ui-datepicker-prev') && !$(e.target).hasClass('ui-datepicker-next')){
				box.slideUp(300, function(){
					hold.removeClass('open');
				});
			}
		});
	});
}

function initAdvanced(){
	$('.filter2').each(function(){
		var hold = $(this);
		var link = hold.find('.toggle-filter');
		var box = hold.find('.adv').not('.next-step');
		var nex = hold.find('.adv.next-step');
		var sim = hold.find('.sim');
		var saved = hold.find('.btn-saved-filters');
		var cancel = hold.find('.btn-cancel');

		saved.click(function(){
			box.slideUp(300);
			nex.slideDown(300);
			return false;
		});

		cancel.click(function(){
			nex.slideUp(300);
			box.slideDown(300);
			return false;
		});

		nex.css({display: 'none'});
		
		link.click(function(){
			if(!hold.hasClass('open')){
				hold.addClass('open');
				box.css({display: 'none'}).slideDown(300);
				sim.css({display:'block'}).slideUp(300);
                $('#rbBulkFilterTypeContains').prop('checked', true);
			}
			else{
				box.slideUp(300, function(){
					hold.removeClass('open');
				});
				nex.slideUp(300);
				sim.slideDown(300);
			}
			return false;
		});
	});
}

function initSearch(){
	$('.search-box').each(function(){
		var hold = $(this);
		var link = hold.find('input.placeholder');
		var check = hold.find('input[data-placeholder]');
		
		var _change = function(){
			if(link.val() == check.not(':checked').data('placeholder')){
				link.val(check.filter(':checked').data('placeholder'));
			}
			link.data('place', check.filter(':checked').data('placeholder'));
		}

		_change();

		check.bind('change', _change);
	});
}

function initFilters(){
	$('.container > .tld').each(function(){
		var hold = $(this);
		var info = $('.search .info');
		var more = hold.find('.more');
		var box = hold.find('.elem > .in');
		var third = hold.find('.elem');
		var check = hold.find('.row:not(.forall) input:checkbox');

		check.change(function(){
			hold.trigger('changeInfo');
		});

		third.each(function(){
			var wrap = $(this);
			var first = wrap.find('.forall input:checkbox');
			var all = wrap.find('.row:not(.forall) input:checkbox');

			first.change(function(){
				if(first.is(':checked')){
					all.prop('checked', true).customForm('refresh');
				}
				else{
					all.prop('checked', false).customForm('refresh');
				}
				hold.trigger('changeInfo');
			});
		});

		hold.bind('changeInfo', function(){
			if(check.filter(':checked').length > 1){
				info.text(check.filter(':checked').length +' selected zones');
			}
			else{
				info.text(check.filter(':checked').length +' selected zone');
			}
		});

		more.click(function(){
                    if (!more.hasClass('open')) {
                        more.addClass('open');
                        hold.addClass('open');
                        box.css({display: 'none', height: 'auto'}).slideDown(300);
                    } else {
                        box.slideUp(300, function(){
                                more.removeClass('open');
                                hold.removeClass('open');
                        });
                    }
			return false;
		});

		hold.trigger('changeInfo');
	});
}

function initFooter(){
	var footer = $('footer.footer');
	var footerPlace = $('div.footer-place');
	var h = footer.outerHeight();
	
	footer.css({marginTop:-h});
	footerPlace.css({height:h});
}

function clearInputs(){
	$('input.placeholder, textarea.placeholder').each(function(){
		var _el = $(this);
		_el.data('place', _el.val());
		
		_el.bind('focus', function(){
			if (this.value == _el.data('place')) {
				this.value = '';
				$(this).removeClass('placeholder');
			}
			$(this).addClass('input-focus');
		}).bind('blur', function(){
			if(this.value == '') {
				this.value = _el.data('place');
				$(this).addClass('placeholder');
			}
			$(this).removeClass('input-focus');
		});
	});
}

function domainIsAllowed(operation, permisions){
    if (parseInt(operation) & parseInt(permisions)) {
        return true;
    } else {
        return false;
    }
}

function clearInputs2(){
	$('div.input2 input, div.input2 textarea').each(function(){
		var _el = $(this);
		var _val = _el.val();
		
		if (_el.parent().hasClass('placeholder')) {
			_el.bind('focus', function(){
				if (this.value == _val) {
					this.value = '';
					$(this).parent().removeClass('placeholder');
				}
				$(this).parent().addClass('focus');
			})
			.bind('blur', function(){
				if(this.value == '') {
					this.value = _val;
					$(this).parent().addClass('placeholder');
				}
				$(this).parent().removeClass('focus');
			});
		}
		else {
			_el.bind('focus', function(){
				$(this).parent().addClass('focus');
				})
			.bind('blur', function(){
				$(this).parent().removeClass('focus');
			});
		};
	});
};

/**
 * jQuery browser v1.0.0
 * Copyright (c) 2013 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

;(function(e,t,n){"use strict";var r,i;e.uaMatch=function(e){e=e.toLowerCase();var t=/(opr)[\/]([\w.]+)/.exec(e)||/(chrome)[ \/]([\w.]+)/.exec(e)||/(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(e)||/(webkit)[ \/]([\w.]+)/.exec(e)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(e)||/(msie) ([\w.]+)/.exec(e)||e.indexOf("trident")>=0&&/(rv)(?::| )([\w.]+)/.exec(e)||e.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(e)||[];var n=/(ipad)/.exec(e)||/(iphone)/.exec(e)||/(android)/.exec(e)||/(windows phone)/.exec(e)||/(win)/.exec(e)||/(mac)/.exec(e)||/(linux)/.exec(e)||[];var r=/(ipad)/.exec(e)||/(iphone)/.exec(e)||/(android)/.exec(e)||/(windows phone)/.exec(e)||[];return{browser:t[3]||t[1]||"",version:t[2]||"0",platform:n[0]||"",mobile:r}};r=e.uaMatch(t.navigator.userAgent);i={};if(r.browser){i[r.browser]=true;i.version=r.version;i.versionNumber=parseFloat(r.version,10)}if(r.platform){i[r.platform]=true}i.mobile=r.mobile.length?true:false;if(i.chrome||i.opr||i.safari){i.webkit=true}if(i.rv){var s="msie";r.browser=s;i[s]=true}if(i.opr){var o="opera";r.browser=o;i[o]=true}i.name=r.browser;i.platform=r.platform;e.browser=i})(jQuery,window);

/**
 * jQuery Custom Form min v1.2.6
 * Copyright (c) 2014 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

jQuery.fn.customForm=jQuery.customForm=function(_options){var _this=this;var methods={destroy:function(){var elements;if(typeof this==="function")elements=$("select, input:radio, input:checkbox");else elements=this.add(this.find("select, input:radio, input:checkbox"));elements.each(function(){var data=$(this).data("customForm");if(data){$(this).removeClass("outtaHere");if(data["events"])data["events"].unbind(".customForm");if(data["create"])data["create"].remove();if(data["resizeElement"])data.resizeElement=
false;$(this).unbind(".customForm")}})},refresh:function(){if(typeof this==="function")$("select, input:radio, input:checkbox").trigger("refresh");else this.trigger("refresh")}};if(typeof _options==="object"||!_options){if(typeof _this=="function")_this=$(document);var options=jQuery.extend(true,{select:{elements:"select.customSelect",structure:'<div class="selectArea"><a href="#" class="selectButton"><span class="center"></span><span class="right">&nbsp;</span></a><div class="disabled"></div></div>',
text:".center",btn:".selectButton",optStructure:'<div class="selectOptions"><ul></ul></div>',maxHeight:false,topClass:"position-top",optList:"ul"},radio:{elements:"input.customRadio",structure:"<div></div>",defaultArea:"radioArea",checked:"radioAreaChecked"},checkbox:{elements:"input.customCheckbox",structure:"<div></div>",defaultArea:"checkboxArea",checked:"checkboxAreaChecked"},disabled:"disabled",hoverClass:"hover"},_options);return _this.each(function(){var hold=jQuery(this);var reset=jQuery();
if(this!==document)reset=hold.find("input:reset, button[type=reset]");initSelect(hold.find(options.select.elements),hold,reset);initRadio(hold.find(options.radio.elements),hold,reset);initCheckbox(hold.find(options.checkbox.elements),hold,reset)})}else if(methods[_options])methods[_options].apply(this);function initSelect(elements,form,reset){elements.not(".outtaHere").each(function(){var select=$(this);var replaced=jQuery(options.select.structure);var selectText=replaced.find(options.select.text);
var selectBtn=replaced.find(options.select.btn);var selectDisabled=replaced.find("."+options.disabled).hide();var optHolder=jQuery(options.select.optStructure);var optList=optHolder.find(options.select.optList);var html="";var optTimer;if(select.prop("disabled"))selectDisabled.show();function createStructure(){html="";select.find("option").each(function(){var selOpt=jQuery(this);if(selOpt.prop("selected"))selectText.html(selOpt.html());html+='<li data-value="'+selOpt.val()+'" '+(selOpt.prop("selected")?
'class="selected"':"")+">"+(selOpt.prop("disabled")?"<span>":'<a href="#">')+selOpt.html()+(selOpt.prop("disabled")?"</span>":"</a>")+"</li>"});if(select.data("placeholder")!==undefined){selectText.html(select.data("placeholder"));replaced.addClass("placeholder")}optList.append(html).find("a").click(function(){replaced.removeClass("placeholder");optList.find("li").removeClass("selected");jQuery(this).parent().addClass("selected");select.val(jQuery(this).parent().data("value").toString());selectText.html(jQuery(this).html());
select.change();replaced.removeClass(options.hoverClass);optHolder.css({left:-9999,top:-9999});return false});if(select.data("customForm")!==undefined&&select.data("customForm")["resizeElement"])select.data("customForm").resizeElement()}createStructure();replaced.width(select.outerWidth());replaced.insertBefore(select);replaced.addClass(select.attr("class"));optHolder.css({width:select.outerWidth(),position:"absolute",left:-9999,top:-9999});optHolder.addClass(select.attr("class"));jQuery(document.body).append(optHolder);
select.bind("refresh",function(){optList.empty();createStructure()});replaced.hover(function(){if(optTimer)clearTimeout(optTimer)},function(){optTimer=setTimeout(function(){replaced.removeClass(options.hoverClass);optHolder.css({left:-9999,top:-9999})},200)});optHolder.hover(function(){if(optTimer)clearTimeout(optTimer)},function(){optTimer=setTimeout(function(){replaced.removeClass(options.hoverClass);optHolder.css({left:-9999,top:-9999})},200)});if(options.select.maxHeight&&optHolder.children().height()>
options.select.maxHeight)optHolder.children().css({height:options.select.maxHeight,overflow:"auto"});selectBtn.click(function(){if(optHolder.offset().left>0){replaced.removeClass(options.hoverClass);optHolder.css({left:-9999,top:-9999})}else{replaced.addClass(options.hoverClass);select.removeClass("outtaHere");optHolder.css({width:select.outerWidth(),top:-9999});select.addClass("outtaHere");if(options.select.maxHeight&&optHolder.children().height()>options.select.maxHeight)optHolder.children().css({height:options.select.maxHeight,
overflow:"auto"});if($(document).height()>optHolder.outerHeight(true)+replaced.offset().top+replaced.outerHeight()){optHolder.removeClass(options.select.topClass).css({top:replaced.offset().top+replaced.outerHeight(),left:replaced.offset().left});replaced.removeClass(options.select.topClass)}else{optHolder.addClass(options.select.topClass).css({top:replaced.offset().top-optHolder.outerHeight(true),left:replaced.offset().left});replaced.addClass(options.select.topClass)}replaced.focus()}return false});
reset.click(function(){setTimeout(function(){select.find("option").each(function(i){var selOpt=jQuery(this);if(selOpt.val()==select.val()){selectText.html(selOpt.html());optList.find("li").removeClass("selected");optList.find("li").eq(i).addClass("selected")}})},10)});select.bind("change.customForm",function(){if(optHolder.is(":hidden"))select.find("option").each(function(i){var selOpt=jQuery(this);if(selOpt.val()==select.val()){selectText.html(selOpt.html());optList.find("li").removeClass("selected");
optList.find("li").eq(i).addClass("selected")}})});select.bind("focus.customForm",function(){replaced.addClass("focus")}).bind("blur.customForm",function(){replaced.removeClass("focus")});select.data("customForm",{"resizeElement":function(){select.removeClass("outtaHere");replaced.width(Math.floor(select.outerWidth()));select.addClass("outtaHere")},"create":replaced.add(optHolder)});$(window).bind("resize.customForm",function(){if(select.data("customForm")["resizeElement"])select.data("customForm").resizeElement()})}).addClass("outtaHere")}
function initRadio(elements,form,reset){elements.each(function(){var radio=$(this);if(!radio.hasClass("outtaHere")&&radio.is(":radio")){radio.data("customRadio",{radio:radio,name:radio.attr("name"),label:$("label[for="+radio.attr("id")+"]").length?$("label[for="+radio.attr("id")+"]"):radio.parents("label"),replaced:jQuery(options.radio.structure,{"class":radio.attr("class")})});radio.data("customRadio").replaced.addClass(radio.attr("class"));var data=radio.data("customRadio");if(radio.is(":disabled")){data.replaced.addClass(options.disabled);
if(radio.is(":checked"))data.replaced.addClass("disabledChecked")}else if(radio.is(":checked")){data.replaced.addClass(options.radio.checked);data.label.addClass("checked")}else{data.replaced.addClass(options.radio.defaultArea);data.label.removeClass("checked")}data.replaced.click(function(){if(jQuery(this).hasClass(options.radio.defaultArea)){radio.change();radio.prop("checked",true);changeRadio(data)}});reset.click(function(){setTimeout(function(){if(radio.is(":checked"))data.replaced.removeClass(options.radio.defaultArea+
" "+options.radio.checked).addClass(options.radio.checked);else data.replaced.removeClass(options.radio.defaultArea+" "+options.radio.checked).addClass(options.radio.defaultArea)},10)});radio.bind("refresh",function(){if(radio.is(":checked")){data.replaced.removeClass(options.radio.defaultArea+" "+options.radio.checked).addClass(options.radio.checked);data.label.addClass("checked")}else{data.replaced.removeClass(options.radio.defaultArea+" "+options.radio.checked).addClass(options.radio.defaultArea);
data.label.removeClass("checked")}});radio.bind("click.customForm",function(){changeRadio(data)});radio.bind("focus.customForm",function(){data.replaced.addClass("focus")}).bind("blur.customForm",function(){data.replaced.removeClass("focus")});data.replaced.insertBefore(radio);radio.addClass("outtaHere");radio.data("customForm",{"create":data.replaced})}})}function changeRadio(data){jQuery('input:radio[name="'+data.name+'"]').not(data.radio).each(function(){var _data=$(this).data("customRadio");if(_data.replaced&&
!jQuery(this).is(":disabled")){_data.replaced.removeClass(options.radio.defaultArea+" "+options.radio.checked).addClass(options.radio.defaultArea);_data.label.removeClass("checked")}});data.replaced.removeClass(options.radio.defaultArea+" "+options.radio.checked).addClass(options.radio.checked);data.label.addClass("checked");data.radio.trigger("change")}function initCheckbox(elements,form,reset){elements.each(function(){var checkbox=$(this);if(!checkbox.hasClass("outtaHere")&&checkbox.is(":checkbox")){checkbox.data("customCheckbox",
{checkbox:checkbox,label:$("label[for="+checkbox.attr("id")+"]").length?$("label[for="+checkbox.attr("id")+"]"):checkbox.parents("label"),replaced:jQuery(options.checkbox.structure,{"class":checkbox.attr("class")})});checkbox.data("customCheckbox").replaced.addClass(checkbox.attr("class"));var data=checkbox.data("customCheckbox");if(checkbox.is(":disabled")){data.replaced.addClass(options.disabled);if(checkbox.is(":checked"))data.replaced.addClass("disabledChecked")}else if(checkbox.is(":checked")){data.replaced.addClass(options.checkbox.checked);
data.label.addClass("checked")}else{data.replaced.addClass(options.checkbox.defaultArea);data.label.removeClass("checked")}data.replaced.click(function(){if(!data.replaced.hasClass("disabled")&&!data.replaced.parents("label").length){if(checkbox.is(":checked"))checkbox.prop("checked",false);else checkbox.prop("checked",true);changeCheckbox(data)}});reset.click(function(){setTimeout(function(){changeCheckbox(data)},10)});checkbox.bind("refresh",function(){if(checkbox.is(":checked")){data.replaced.removeClass(options.checkbox.defaultArea+
" "+options.checkbox.defaultArea).addClass(options.checkbox.checked);data.label.addClass("checked")}else{data.replaced.removeClass(options.checkbox.defaultArea+" "+options.checkbox.checked).addClass(options.checkbox.defaultArea);data.label.removeClass("checked")}});checkbox.bind("click.customForm",function(){changeCheckbox(data)});checkbox.bind("focus.customForm",function(){data.replaced.addClass("focus")}).bind("blur.customForm",function(){data.replaced.removeClass("focus")});data.replaced.insertBefore(checkbox);
checkbox.addClass("outtaHere");data.replaced.parents("label").bind("click.customForm",function(){if(!data.replaced.hasClass("disabled")){if(checkbox.is(":checked"))checkbox.prop("checked",false);else checkbox.prop("checked",true);changeCheckbox(data)}return false});checkbox.data("customForm",{"create":data.replaced,"events":data.replaced.parents("label")})}})}function changeCheckbox(data){if(data.checkbox.is(":checked")){data.replaced.removeClass(options.checkbox.defaultArea+" "+options.checkbox.defaultArea).addClass(options.checkbox.checked);
data.label.addClass("checked")}else{data.replaced.removeClass(options.checkbox.defaultArea+" "+options.checkbox.checked).addClass(options.checkbox.defaultArea);data.label.removeClass("checked")}data.checkbox.trigger("change")}};

/**
 * jQuery gallery v2.2.0
 * Copyright (c) 2013 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

;(function($){var _installDirections=function(data){data.holdWidth=data.list.parent().outerWidth();data.woh=data.elements.outerWidth(true);if(!data.direction)data.parentSize=data.holdWidth;else{data.woh=data.elements.outerHeight(true);data.parentSize=data.list.parent().height();}data.wrapHolderW=Math.ceil(data.parentSize/data.woh);if(((data.wrapHolderW-1)*data.woh+data.woh/2)>data.parentSize)data.wrapHolderW--;if(data.wrapHolderW==0)data.wrapHolderW=1;},_dirAnimate=function(data){if(!data.direction)return{left:-(data.woh*data.active)};else return{top:-(data.woh*data.active)};},_initDisableBtn=function(data){data.prevBtn.removeClass(data.disableBtn);data.nextBtn.removeClass(data.disableBtn);if(data.active==0||data.count+1==data.wrapHolderW-1)data.prevBtn.addClass(data.disableBtn);if(data.active==0&&data.count+1==1||data.count+1<=data.wrapHolderW-1)data.nextBtn.addClass(data.disableBtn);if(data.active==data.rew)data.nextBtn.addClass(data.disableBtn);},_initEvent=function(data,btn,side){btn.bind(data.event+'.gallery',function(){if(data.flag){if(data.infinite)data.flag=false;if(data._t)clearTimeout(data._t);_toPrepare(data,side);if(data.autoRotation)_runTimer(data);if(typeof data.onChange=='function')data.onChange({elements:data.elements,active:data.active});}if(data.event=='click')return false;});},_initEventSwitcher=function(data){data.switcher.bind(data.event+'.gallery',function(){if(data.flag&&!$(this).hasClass(data.activeClass)){if(data.infinite)data.flag=false;data.active=data.switcher.index(jQuery(this))*data.slideElement;if(data.infinite)data.active=data.switcher.index(jQuery(this))+data.count;if(data._t)clearTimeout(data._t);if(data.disableBtn)_initDisableBtn(data);if(!data.effect)_scrollElement(data);else _fadeElement(data);if(data.autoRotation)_runTimer(data);if(typeof data.onChange=='function')data.onChange({elements:data.elements,active:data.active});}if(data.event=='click')return false;});},_toPrepare=function(data,side){if(!data.infinite){if((data.active==data.rew)&&data.circle&&side)data.active=-data.slideElement;if((data.active==0)&&data.circle&&!side)data.active=data.rew+data.slideElement;for(var i=0;i<data.slideElement;i++){if(side){if(data.active+1<=data.rew)data.active++;}else{if(data.active-1>=0)data.active--;}};}else{if(data.active>=data.count+data.count&&side)data.active-=data.count;if(data.active<=data.count-1&&!side)data.active+=data.count;data.list.css(_dirAnimate(data));if(side)data.active+=data.slideElement;else data.active-=data.slideElement;}if(data.disableBtn)_initDisableBtn(data);if(!data.effect)_scrollElement(data);else _fadeElement(data);},_fadeElement=function(data){if(!data.IEfx&&data.IE){data.list.eq(data.last).css({opacity:0});data.list.removeClass(data.activeClass).eq(data.active).addClass(data.activeClass).css({opacity:'auto'});}else{data.list.removeClass(data.activeClass).css({zIndex:1});data.list.eq(data.last).stop().css({zIndex:2,opacity:1});data.list.eq(data.active).addClass(data.activeClass).css({opacity:0,zIndex:3}).animate({opacity:1},{queue:false,duration:data.duration,complete:function(){jQuery(this).css('opacity','auto');}});}if(data.autoHeight)data.list.parent().animate({height:data.list.eq(data.active).outerHeight()},{queue:false,duration:data.duration});if(data.switcher)data.switcher.removeClass(data.activeClass).eq(data.active).addClass(data.activeClass);data.last=data.active;},_scrollElement=function(data){if(!data.infinite)data.list.animate(_dirAnimate(data),{queue:false,easing:data.easing,duration:data.duration});else{data.list.animate(_dirAnimate(data),data.duration,data.easing,function(){if(data.active>=data.count+data.count)data.active-=data.count;if(data.active<=data.count-1)data.active+=data.count;data.list.css(_dirAnimate(data));data.flag=true;});}if(data.autoHeight)data.list.parent().animate({height:data.list.children().eq(data.active).outerHeight()},{queue:false,duration:data.duration});if(data.switcher){if(!data.infinite)data.switcher.removeClass(data.activeClass).eq(Math.ceil(data.active/data.slideElement)).addClass(data.activeClass);else{data.switcher.removeClass(data.activeClass).eq(data.active-data.count).addClass(data.activeClass);data.switcher.removeClass(data.activeClass).eq(data.active-data.count-data.count).addClass(data.activeClass);data.switcher.eq(data.active).addClass(data.activeClass);}}},_runTimer=function(data){if(data._t)clearTimeout(data._t);data._t=setInterval(function(){if(data.infinite)data.flag=false;_toPrepare(data,true);if(typeof data.onChange=='function')data.onChange({elements:data.elements,active:data.active});},data.autoRotation);},_rePosition=function(data){if(data.flexible&&!data.direction){_installDirections(data);if(data.elements.length*data.minWidth>data.holdWidth){data.elements.css({width:Math.floor(data.holdWidth/Math.floor(data.holdWidth/data.minWidth))});if(data.elements.outerWidth(true)>Math.floor(data.holdWidth/Math.floor(data.holdWidth/data.minWidth))){data.elements.css({width:Math.floor(data.holdWidth/Math.floor(data.holdWidth/data.minWidth))-(data.elements.outerWidth(true)-Math.floor(data.holdWidth/Math.floor(data.holdWidth/data.minWidth)))});}}else{data.active=0;data.elements.css({width:Math.floor(data.holdWidth/data.elements.length)});}}_installDirections(data);if(!data.effect){data.rew=data.count-data.wrapHolderW+1;if(data.active>data.rew)data.active=data.rew;data.list.css({position:'relative'}).css(_dirAnimate(data));if(data.switcher)data.switcher.removeClass(data.activeClass).eq(data.active).addClass(data.activeClass);if(data.autoHeight)data.list.parent().css({height:data.list.children().eq(data.active).outerHeight()});}else{data.rew=data.count;data.list.css({opacity:0}).removeClass(data.activeClass).eq(data.active).addClass(data.activeClass).css({opacity:1}).css('opacity','auto');if(data.switcher)data.switcher.removeClass(data.activeClass).eq(data.active).addClass(data.activeClass);if(data.autoHeight)data.list.parent().css({height:data.list.eq(data.active).outerHeight()});}if(data.disableBtn)_initDisableBtn(data);},_initTouchEvent=function(data){var span=$("<span></span>");var touchOnGallery=false;var startTouchPos;var listPosNow;var side;var start;span.css({position:"absolute",left:0,top:0,width:9999,height:9999,cursor:"pointer",zIndex:9999,display:"none"});data.list.parent().css({position:"relative"}).append(span);data.list.bind("mousedown.gallery touchstart.gallery",function(e){touchOnGallery=true;startTouchPos=e.originalEvent.touches?e.originalEvent.touches[0].pageX:e.pageX;data.list.stop();start=0;listPosNow=data.list.position().left;if(e.type=="mousedown")e.preventDefault();});$(document).bind("mousemove.gallery touchmove.gallery",function(e){if(touchOnGallery&&Math.abs(startTouchPos-(e.originalEvent.touches?e.originalEvent.touches[0].pageX:e.pageX))>10){span.css({display:"block"});start=(e.originalEvent.touches?e.originalEvent.touches[0].pageX:e.pageX)-startTouchPos;if(!data.effect){data.list.css({left:listPosNow+start});}return false;}}).bind("mouseup.gallery touchend.gallery",function(e){if(touchOnGallery){span.css({display:"none"});if(!data.infinite){if(!data.effect){if(data.list.position().left>0){data.active=0;_scrollElement(data);}else if(data.list.position().left<-data.woh*data.rew){data.active=data.rew;_scrollElement(data);}else{data.active=Math.floor(data.list.position().left/-data.woh);if(start<0){data.active+=1;}_scrollElement(data);}}else{if(start<0){_toPrepare(data,true);}if(start>0){_toPrepare(data,false);}}}else{if(data.list.position().left>-data.woh*data.count){data.list.css({left:data.list.position().left-data.woh*data.count});}if(data.list.position().left<-data.woh*data.count*2){data.list.css({left:data.list.position().left+data.woh*data.count});}data.active=Math.floor(data.list.position().left/-data.woh);if(start<0){data.active+=1;}_scrollElement(data);}if(data.disableBtn)_initDisableBtn(data);if(typeof data.onChange=="function")data.onChange({elements:data.elements,active:data.active});if(data.autoRotation)_runTimer(data);touchOnGallery=false;}});},methods={init:function(options){return this.each(function(){var $this=$(this);$this.data('gallery',jQuery.extend({},defaults,options));var data=$this.data('gallery');data.aR=data.autoRotation;data.context=$this;data.list=data.context.find(data.listOfSlides);data.elements=data.list;if(data.elements.css('position')=='absolute'&&data.autoDetect)data.effect=true;data.count=data.list.index(data.list.filter(':last'));if(!data.IEfx)data.IE=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase());if(!data.effect)data.list=data.list.parent();if(data.switcher)data.switcher=data.context.find(data.switcher);if(data.switcher.length==0)data.switcher=false;if(data.nextBtn)data.nextBtn=data.context.find(data.nextBtn);if(data.prevBtn)data.prevBtn=data.context.find(data.prevBtn);if(data.switcher)data.active=data.switcher.index(data.switcher.filter('.'+data.activeClass+':eq(0)'));else data.active=data.elements.index(data.elements.filter('.'+data.activeClass+':eq(0)'));if(data.active<0)data.active=0;data.last=data.active;if(data.flexible&&!data.direction)data.minWidth=data.elements.outerWidth(true);_rePosition(data);if(data.flexible&&!data.direction){$(window).bind('resize.gallery',function(){_rePosition(data);});}data.flag=true;if(data.infinite){data.count++;data.active+=data.count;data.list.append(data.elements.clone());data.list.append(data.elements.clone());data.list.css(_dirAnimate(data));data.elements=data.list.children();}if(data.rew<0&&!data.effect){data.list.css({left:0});return false;}if(data.nextBtn)_initEvent(data,data.nextBtn,true);if(data.prevBtn)_initEvent(data,data.prevBtn,false);if(data.switcher)_initEventSwitcher(data);if(data.autoRotation)_runTimer(data);if(data.touch)_initTouchEvent(data);});},option:function(name,set){if(set){return this.each(function(){var data=$(this).data('gallery');if(data)data[name]=set;});}else{var ar=[];this.each(function(){var data=$(this).data('gallery');if(data)ar.push(data[name]);});if(ar.length>1)return ar;else return ar[0];}},destroy:function(){return this.each(function(){var $this=$(this),data=$this.data('gallery');if(data){if(data._t)clearTimeout(data._t);data.context.find('*').unbind('.gallery');$(window).unbind('.gallery');$(document).unbind('.gallery');data.elements.removeAttr('style');data.list.removeAttr('style');$this.removeData('gallery');}});},rePosition:function(){return this.each(function(){var $this=$(this),data=$this.data('gallery');_rePosition(data);});},stop:function(){return this.each(function(){var $this=$(this),data=$this.data('gallery');data.aR=data.autoRotation;data.autoRotation=false;if(data._t)clearTimeout(data._t);});},play:function(time){return this.each(function(){var $this=$(this),data=$this.data('gallery');if(data._t)clearTimeout(data._t);data.autoRotation=time?time:data.aR;if(data.autoRotation)_runTimer(data);});},next:function(element){return this.each(function(){var $this=$(this),data=$this.data('gallery');if(element!='undefined'&&element>-1){data.active=element;if(data.disableBtn)_initDisableBtn(data);if(!data.effect)_scrollElement(data);else _fadeElement(data);}else{if(data.flag){if(data.infinite)data.flag=false;if(data._t)clearTimeout(data._t);_toPrepare(data,true);if(data.autoRotation)_runTimer(data);if(typeof data.onChange=='function')data.onChange({elements:data.elements,active:data.active});}}});},prev:function(){return this.each(function(){var $this=$(this),data=$this.data('gallery');if(data.flag){if(data.infinite)data.flag=false;if(data._t)clearTimeout(data._t);_toPrepare(data,false);if(data.autoRotation)_runTimer(data);if(typeof data.onChange=='function')data.onChange({elements:data.elements,active:data.active});}});}},defaults={infinite:false,activeClass:'active',duration:300,slideElement:1,autoRotation:false,effect:false,listOfSlides:'ul:eq(0) > li',switcher:false,disableBtn:false,nextBtn:'a.link-next, a.btn-next, .next',prevBtn:'a.link-prev, a.btn-prev, .prev',IEfx:true,circle:true,direction:false,event:'click',autoHeight:false,easing:'easeOutQuad',flexible:false,autoDetect:true,touch:true,onChange:null};$.fn.gallery=function(method){if(methods[method]){return methods[method].apply(this,Array.prototype.slice.call(arguments,1));}else{if(typeof method==='object'||!method){return methods.init.apply(this,arguments);}else{$.error('Method '+method+' does not exist on jQuery.gallery');}}};jQuery.easing['jswing']=jQuery.easing['swing'];jQuery.extend(jQuery.easing,{def:'easeOutQuad',swing:function(x,t,b,c,d){return jQuery.easing[jQuery.easing.def](x,t,b,c,d);},easeOutQuad:function(x,t,b,c,d){return-c*(t/=d)*(t-2)+b;},easeOutCirc:function(x,t,b,c,d){return c*Math.sqrt(1-(t=t/d-1)*t)+b;}});})(jQuery);
;(function(e,t,n){"use strict";var r,i;e.uaMatch=function(e){e=e.toLowerCase();var t=/(opr)[\/]([\w.]+)/.exec(e)||/(chrome)[ \/]([\w.]+)/.exec(e)||/(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(e)||/(webkit)[ \/]([\w.]+)/.exec(e)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(e)||/(msie) ([\w.]+)/.exec(e)||e.indexOf("trident")>=0&&/(rv)(?::| )([\w.]+)/.exec(e)||e.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(e)||[];var n=/(ipad)/.exec(e)||/(iphone)/.exec(e)||/(android)/.exec(e)||/(windows phone)/.exec(e)||/(win)/.exec(e)||/(mac)/.exec(e)||/(linux)/.exec(e)||[];var r=/(ipad)/.exec(e)||/(iphone)/.exec(e)||/(android)/.exec(e)||/(windows phone)/.exec(e)||[];return{browser:t[3]||t[1]||"",version:t[2]||"0",platform:n[0]||"",mobile:r}};r=e.uaMatch(t.navigator.userAgent);i={};if(r.browser){i[r.browser]=true;i.version=r.version;i.versionNumber=parseFloat(r.version,10)}if(r.platform){i[r.platform]=true}i.mobile=r.mobile.length?true:false;if(i.chrome||i.opr||i.safari){i.webkit=true}if(i.rv){var s="msie";r.browser=s;i[s]=true}if(i.opr){var o="opera";r.browser=o;i[o]=true}i.name=r.browser;i.platform=r.platform;e.browser=i})(jQuery,window);

/**
 * jQuery simplebox v2.0.4
 * Copyright (c) 2013 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

;(function($){var _condition=function(id,options){if($.simplebox.modal){var data=$.simplebox.modal.data("simplebox");data.onClose($.simplebox.modal);$.simplebox.modal.fadeOut(data.duration,function(){$.simplebox.modal.css({left:"-9999px",top:"-9999px"}).show();data.afterClose($.simplebox.modal);$.simplebox.modal.removeData("simplebox");$.simplebox.modal=false;_toPrepare(id,options)})}else _toPrepare(id,options)},_calcWinWidth=function(){return $(document).width()>$("body").width()?$(document).width():
jQuery("body").width()},_toPrepare=function(id,options){$.simplebox.modal=$(id);$.simplebox.modal.data("simplebox",options);var data=$.simplebox.modal.data("simplebox");data.btnClose=$.simplebox.modal.find(data.linkClose);var popupTop=$(window).scrollTop()+$(window).height()/2-$.simplebox.modal.outerHeight(true)/2;if($(window).scrollTop()>popupTop)popupTop=$(window).scrollTop();if(popupTop+$.simplebox.modal.outerHeight(true)>$(document).height())popupTop=$(document).height()-$.simplebox.modal.outerHeight(true);
if(popupTop<0)popupTop=0;if(!data.positionFrom)$.simplebox.modal.css({zIndex:1E3,top:data.top!==false?data.top:popupTop,left:_calcWinWidth()/2-$.simplebox.modal.outerWidth(true)/2}).hide();else $.simplebox.modal.css({zIndex:1E3,top:$(data.positionFrom).offset().top+$(data.positionFrom).outerHeight(true),left:$(data.positionFrom).offset().left}).hide();_initAnimate(data);_closeEvent(data,data.btnClose);if(data.overlay.closeClick)_closeEvent(data,$.simplebox.overlay)},_initAnimate=function(data){data.onOpen($.simplebox.modal);
if(data.overlay)$.simplebox.overlay.css({background:data.overlay.color,opacity:data.overlay.opacity}).fadeIn(data.duration,function(){$.simplebox.modal.fadeIn(data.duration,function(){$.simplebox.busy=false;data.afterOpen($.simplebox.modal);if($(window).scrollTop()>$.simplebox.modal.offset().top)$("html, body").animate({scrollTop:$.simplebox.modal.offset().top},500)})});else{$.simplebox.overlay.fadeOut(data.duration);$.simplebox.modal.fadeIn(data.duration,function(){$.simplebox.busy=false;data.afterOpen($.simplebox.modal);
if($(window).scrollTop()>$.simplebox.modal.offset().top)$("html, body").animate({scrollTop:$.simplebox.modal.offset().top},500)})}},_closeEvent=function(data,element){element.unbind("click.simplebox").bind("click.simplebox",function(){if(!$.simplebox.busy){$.simplebox.busy=true;data.onClose($.simplebox.modal);$.simplebox.modal.fadeOut(data.duration,function(){$.simplebox.modal.css({left:"-9999px",top:"-9999px"}).show();$.simplebox.overlay.fadeOut(data.duration,function(){data.afterClose($.simplebox.modal);
$.simplebox.modal.removeData("simplebox");$.simplebox.modal=false;$.simplebox.busy=false})})}return false})},methods={init:function(options){$(this).unbind("click.simplebox").bind("click.simplebox",function(){var data=$(this).data("simplebox");if(!$(this).hasClass(defaults.disableClass)&&!$.simplebox.busy){$.simplebox.busy=true;_condition($(this).attr("href")?$(this).attr("href"):$(this).data("href"),jQuery.extend(true,{},defaults,options))}return false});return this},option:function(name,set){if(set)return this.each(function(){var data=
$(this).data("simplebox");if(data)data[name]=set});else{var ar=[];this.each(function(){var data=$(this).data("simplebox");if(data)ar.push(data[name])});if(ar.length>1)return ar;else return ar[0]}}},defaults={duration:300,linkClose:".close, .btn-close",top:false,disableClass:"disabled",overlay:{box:"simplebox-overlay",color:"black",closeClick:true,opacity:.8},positionFrom:false,onOpen:function(){},afterOpen:function(){},onClose:function(){},afterClose:function(){}};$.fn.simplebox=function(method){if(methods[method])return methods[method].apply(this,
Array.prototype.slice.call(arguments,1));else if(typeof method==="object"||!method)return methods.init.apply(this,arguments);else $.error("Method "+method+" does not exist on jQuery.simplebox")};$.simplebox=function(id,options){if(!$.simplebox.busy){$.simplebox.busy=true;_condition(id,jQuery.extend(true,{},defaults,options))}};$.simplebox.init=function(){if(!$.simplebox.overlay){$.simplebox.overlay=jQuery('<div class="'+defaults.overlay.box+'"></div>');jQuery("body").append($.simplebox.overlay);$.simplebox.overlay.css({position:"fixed",
zIndex:999,left:0,top:0,width:"100%",height:"100%",background:defaults.overlay.color,opacity:defaults.overlay.opacity}).hide()}$(document).unbind("keypress.simplebox").bind("keypress.simplebox",function(e){if($.simplebox.modal&&$.simplebox.modal.is(":visible")&&e.keyCode==27)$.simplebox.close()});$(window).bind("resize.simplebox",function(){if($.simplebox.modal&&$.simplebox.modal.is(":visible")){var data=$.simplebox.modal.data("simplebox");if(!data.positionFrom)$.simplebox.modal.animate({left:_calcWinWidth()/
2-$.simplebox.modal.outerWidth(true)/2},{queue:false,duration:$.simplebox.modal.data("simplebox").duration});else $.simplebox.modal.animate({top:$(data.positionFrom).offset().top+$(data.positionFrom).outerHeight(true),left:$(data.positionFrom).offset().left},{queue:false,duration:$.simplebox.modal.data("simplebox").duration})}})};$.simplebox.close=function(){if($.simplebox.modal&&!$.simplebox.busy){var data=$.simplebox.modal.data("simplebox");$.simplebox.busy=true;data.onClose($.simplebox.modal);
$.simplebox.modal.fadeOut(data.duration,function(){$.simplebox.modal.css({left:"-9999px",top:"-9999px"}).show();if($.simplebox.overlay)$.simplebox.overlay.fadeOut(data.duration,function(){data.afterClose($.simplebox.modal);$.simplebox.modal.removeData("simplebox");$.simplebox.modal=false;$.simplebox.busy=false});else{data.afterClose($.simplebox.modal);$.simplebox.modal.removeData("simplebox");$.simplebox.modal=false;$.simplebox.busy=false}})}};$(document).ready(function(){$.simplebox.init()})})(jQuery);

/**
 * jQuery tabs min v1.0.0
 * Copyright (c) 2011 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

jQuery.fn.tabs=function(options){return new Tabs(this.get(0),options);};function Tabs(context,options){this.init(context,options);}Tabs.prototype={options:{},init:function(context,options){this.options=jQuery.extend({listOfTabs:'a.tab',active:'active',event:'click'},options||{});this.btn=jQuery(context).find(this.options.listOfTabs);this.last=this.btn.index(this.btn.filter('.'+this.options.active));if(this.last==-1)this.last=0;this.btn.removeClass(this.options.active).eq(this.last).addClass(this.options.active);var _this=this;this.btn.each(function(i){if(_this.last==i)jQuery($(this).attr('href')).show();else jQuery($(this).attr('href')).hide();});this.initEvent(this,this.btn);},initEvent:function($this,el){el.bind(this.options.event,function(){if($this.last!=el.index(jQuery(this)))$this.changeTab(el.index(jQuery(this)));return false;});},changeTab:function(ind){jQuery(this.btn.eq(this.last).attr('href')).hide();jQuery(this.btn.eq(ind).attr('href')).show();this.btn.eq(this.last).removeClass(this.options.active);this.btn.eq(ind).addClass(this.options.active);this.last=ind;}}
