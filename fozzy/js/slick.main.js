$(document).ready(function(){
  $('.prices.view2.to-slide > .hold').slick({
    dots: true,
	infinite: false,
	slidesToShow: 2,
	slidesToScroll: 2,
	//swipeToSlide: true,
	arrows: false,
	variableWidth: true,
	//autoplay: true,
	//autoplaySpeed: 2000,
	responsive: [
    {
      breakpoint: 760,
      settings: {
		slidesToShow: 1,
		slidesToScroll: 1,
        swipeToSlide: true
      }
    }
  ]
  });
  $('.prices.to-slide > .hold').slick({
    dots: true,
	infinite: false,
	slidesToShow: 1,
	slidesToScroll: 2,
	//swipeToSlide: true,
	arrows: false,
	variableWidth: true,
	//autoplay: true,
	//autoplaySpeed: 2000,
	responsive: [
    {
      breakpoint: 760,
      settings: {
		slidesToScroll: 1,
        swipeToSlide: true
      }
    }
  ]
  });
});