$(document).ready(function(){
	if (navigator.userAgent.toLowerCase().indexOf('windows') != -1) $('body').addClass('windows');
    $('.menu > ul > li > .grad1').gradientText({
        colors: ['#ff7d00', '#ff8300']
    });
    $('.menu > ul > li > .grad2').gradientText({
        colors: ['#ff8a00', '#ff9900']
    });
    $('.menu > ul > li > .grad3').gradientText({
        colors: ['#ffa100', '#ffae00']
    });
    $('.menu > ul > li > .grad4').gradientText({
        colors: ['#ffb600', '#ffc100']
    });
    $('.menu > ul > li > .grad5').gradientText({
        colors: ['#69b440', '#53bd82']
    });
    $('.menu > ul > li > .grad6').gradientText({
        colors: ['#49c1a0', '#40c5ba']
    });
    $('.menu .sub li > a, .menu .sub li > span').gradientText({
        colors: ['#5d9d3a', '#3cafa6']
    });
    $('.colored-orange').gradientText({
        colors: ['#ff7d00', '#ffc100']
    });
    $('.top-text .color').gradientText({
        colors: ['#ff7d00', '#ffc100']
    });
    $('.lang ul li > a > span').gradientText({
        colors: ['#f6a603', '#ffc500']
    });
    $('.lang2 li > a > span').gradientText({
        colors: ['#f6a603', '#ffc500']
    });
    $('.fastest, .fastest h2 > span').gradientText({
        colors: ['#ff7d00', '#ffc100']
    });
    $('.fastest h2 > span').gradientText({
        colors: ['#ff7d00', '#ffc100']
    });
    $('.fastest h2 strong').gradientText({
        colors: ['#ff7d00', '#ffc100']
    });
    $('.benefits h2').gradientText({
        colors: ['#88d46d', '#1fb8f4']
    });
    $('.benefits .l h3').gradientText({
        colors: ['#78d082', '#6fcea1']
    });
    $('.benefits .r h3').gradientText({
        colors: ['#5ecab7', '#31bddd']
    });
    $('.benefits .for-mob h3').gradientText({
        colors: ['#78d082', '#31bddd']
    });
    $('.about h2').gradientText({
        colors: ['#ff7e7e', '#ffba00']
    });
    $('.menu2 .color1 > ul > li > a').gradientText({
        colors: ['#65ad3e', '#40c5ba']
    });
    $('.menu2 .color2 > ul > li > a').gradientText({
        colors: ['#bf9142', '#e3bc7e']
    });
    $('.menu2 .color3 > ul > li > a').gradientText({
        colors: ['#b1535a', '#8c6c9c']
    });
    $('.top-text2, .top-text8, .top-text6 .color').gradientText({
        colors: ['#ff7d00', '#ffc100']
    });
    $('.top-text3.big, .top-text3.color').gradientText({
        colors: ['#ff7d00', '#ffc100']
    });
    $('.top-text3.big div, .top-text3.color div').gradientText({
        colors: ['#ff7d00', '#ffc100']
    });
    $('.order .head h1, .order .head .h1').gradientText({
        colors: ['#ff7d00', '#ffc100']
    });
	$('ul.tabs').each(function(){
		$(this).tabs();
	});
	$('div.tabs2').each(function(){
		$(this).tabs();
	});
	
	initHeader();
	openClose();
	moreList();
	$('.simplebox').simplebox();
	clearInputs();
	clearInputs2();
	initCustomSelect();
	if (!$.browser.mobile) {
		if(!($.browser.msie && $.browser.version < 9)) {
			if(typeof $.fn.parallax == 'function') $('.parallax').parallax();
		}
	}
	checkSearch();
	initSlide();
	$('.go-to').click(function(){
		$('body, html').animate({scrollTop: $($(this).attr('href')).offset().top}, {queue:false, duration: 700});
		return false;
	});
	pageInit.animStart({
		divider: 6,
		box: '.start-animation:not(.new-anim)',
		width: 1200,
		className: 'visible'
	});
	pageInit.animStart({
		divider: 14,
		box: '.new-anim',
		width: 1200,
		className: 'visible'
	});
	scrollMove();
	bigGall();
	$('.slick-slider').slick({
		arrows: false,
		infinite: false,
		dots: true,
		slidesToShow:3,
		slidesToScroll:1,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow:2
				}
			},
			{
				breakpoint: 760,
				settings: {
					slidesToShow:1
				}
			}
		]
	});
});

function bigGall () {
	$('.index-new-block.testimonials-new').each(function(){
		var hold = $(this);
		var gall = hold.find('.testi-slider');
		var bubble = hold.find('.bubble'), data;
		var _resize = function () {
			data.elements.css({width: data.list.parent().outerWidth()});
			gall.gallery('rePosition');
		}
		
		bubble.gallery({
			effect: 'transparent'
		});
		gall.gallery({
			switcher: ".dots > span",
			onChange: function (obj) {
				bubble.gallery('next', obj.data.active);
			}
		});
		data = gall.data('gallery');

		_resize();
		$(window).bind('resize', _resize);
	});
}

function scrollMove () {
	$('.scroll-move').each(function(){
		var hold = $(this);
		var left = hold.find('.bgl');
		var right = hold.find('.bgr');
		var win = $(window), top;
		
		var _scroll = function () {
			top = hold.offset().top - $(window).height()/1.5;
			if(win.scrollTop() > top){
				left.css({
					left: 100 - (win.scrollTop() - top)/2 >= 0 ? 100 - (win.scrollTop() - top)/2 : 0,
					bottom: -50 + (win.scrollTop() - top)/4 <= 0 ? -50 + (win.scrollTop() - top)/4 : 0
				});
				right.css({
					right: 100 - (win.scrollTop() - top)/2 > 0 ? 100 - (win.scrollTop() - top)/2 : 0,
					bottom: -50 + (win.scrollTop() - top)/4 <= 0 ? -50 + (win.scrollTop() - top)/4 : 0
				});
			}
			else{
				left.css({
					left: 100,
					bottom: -50
				});
				right.css({
					right: 100,
					bottom: -50
				});
			}
		}
		_scroll();
		$(window).bind('scroll', _scroll);
	});
}

var pageInit = {
	init: function(){
		this.animStart({
			divider: 5,
			box: '.start-animation',
			width: 1200,
			className: 'visible'
		});
	},
	
	animStart: function(obj){
		var box = $(obj.box);
		if(box.length == 0) return false;

		box.each(function(i){
			var hold = $(this);
			var k = hold.data('number');

			if(k){
				var delay = hold.data('delay') || 0;
				var time, date, newDate = 0, timer = hold.data('duration') || 1000;
				
				hold.bind(obj.className, function(){
					setTimeout(function(){
						date = new Date().getTime();
						time = setInterval(function(){
							hold.text(Math.round(k/timer*newDate));
							newDate = new Date().getTime() - date;
							if(newDate >= timer) {
								hold.text(k);
								clearTimeout(time);
							}
						}, 20);
					}, ($(window).width() >= obj.width ? delay : 0));
				});
			}
		});

		var all = function(){
			box.not('.'+obj.className).each(function(){
				if ($(this).offset().top  <= $(window).scrollTop() + $(window).height()-($(window).height()/obj.divider)) {
					$(this).addClass(obj.className).trigger(obj.className);
				}
			});
		}
		$(window).bind('scroll resize', all);
		all();
	}
}

function initHeader () {
	$('header.header-new').each(function(){
		var hold = $(this);
		var lineTop = hold.position().top;

		var _scroll = function () {
			if($(window).scrollTop() > lineTop){
				$('body').addClass('fixed-header');
			}
			else{
				$('body').removeClass('fixed-header');
			}
		}
		_scroll();
		$(window).bind('scroll', _scroll);
	});
}

function initSlide () {
	$('.check-block').each(function(){
		var hold = $(this);
		var link = hold.find('input.check-radio');
		var block;
		var _change = function () {
			link.each(function(){
				block = $(this).data('block');
				if(block){
					if($(this).is(':checked')){
						hold.find(block).slideDown(500);
					}
					else{
						hold.find(block).slideUp(500);
					}
				}
			});
		}
		_change();
		link.change(_change);
	});
}

$(document).ready(function(){
	$(".link-scroll").on("click", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
		top = $(id).offset().top;
		$('body,html').animate({scrollTop: top}, 500);
	});
});

function checkSearch () {
	$('.check-search').each(function(){
		var hold = $(this);
		var inputName = hold.find('input.name');
		var inputTld = hold.find('input.tld');
		var inputDomain = hold.find('input.domain');
		
		inputName.on('keypress keyup keydown blur paste', function(event) {
			inputDomain.val(inputName.val().substr(0, inputName.val().lastIndexOf('.')));
			if(inputName.val().lastIndexOf('.') != -1) {
				inputTld.val(inputName.val().substr(inputName.val().lastIndexOf('.')));
			}
			else inputTld.val('');
		});
	});
}

function initCustomSelect () {
	$('.check-avail .custom-select').each(function(){
		var hold = $(this);
		var link = hold.find('.toggle');
		var input = hold.find('input.choise');
		var options = hold.find('a.option');
		var box = hold.find('.sub2');
		
		link.find('.txt').text(input.val()).attr('title', input.val());

		link.bind('mousedown', function(){
			if(!hold.hasClass('open')){
				hold.addClass('open');
				box.css({display: 'none'}).slideDown(300);
			}
			else{
				box.css({display: 'block'}).slideUp(300, function(){
					hold.removeClass('open');
				});
			}
			return false;
		});

		options.click(function(){
			input.val($(this).text());
			link.find('.txt').text($(this).text()).attr('title', $(this).text());
			box.css({display: 'block'}).slideUp(300, function(){
				hold.removeClass('open');
			});
			return false;
		});
		
		$(document).bind('click touchstart mousedown', function(e){
			if(!($(e.target).parents().index(hold) != -1 || $(e.target).index(hold) != -1)){
				box.slideUp(300, function(){
					hold.removeClass('open');
				});
			}
		});
	});
}

function clearInputs(){
	$('div.input2 input, div.input2 textarea').each(function(){
		var _el = $(this);
		var _val = _el.val();
		
		if (_el.parent().hasClass('placeholder')) {
			_el.bind('focus', function(){
				if (this.value == _val) {
					this.value = '';
					$(this).parent().removeClass('placeholder');
				}
				$(this).parent().addClass('focus');
			})
			.bind('blur', function(){
				if(this.value == '') {
					this.value = _val;
					$(this).parent().addClass('placeholder');
				}
				$(this).parent().removeClass('focus');
			});
		}
		else {
			_el.bind('focus', function(){
				$(this).parent().addClass('focus');
				})
			.bind('blur', function(){
				$(this).parent().removeClass('focus');
			});
		};
	});
};

function clearInputs2(){
	$('input.placeholder, textarea.placeholder').each(function(){
		var _el = $(this);
		var _val = _el.val();
		
		_el.bind('focus', function(){
			if (this.value == _val) {
				this.value = '';
				$(this).removeClass('placeholder');
			}
			$(this).addClass('input-focus');
		}).bind('blur', function(){
			if(this.value == '') {
				this.value = _val;
				$(this).addClass('placeholder');
			}
			$(this).removeClass('input-focus');
		});
	});
}
function openClose(){
	$('.container .tld').each(function(){
		var hold = $(this);
		var link = hold.find('.toggle');
		var box = hold.find('.sub');
		var tld = link.find('> div');
		var tldLink = box.find('.fl ul > li > a');
		var inputLtd = hold.find('input:hidden');
				
		box.css({display: 'none'});
		hold.removeClass('open');

		link.unbind('click').click(function(){
			if(!hold.hasClass('open')){
				hold.addClass('open');
				box.slideDown(300);
			}
			else{
				box.slideUp(300, function(){
					hold.removeClass('open');
				});
			}
			return false;
		});
		
		tldLink.on('click', function(){
			tld.text($(this).text());
			inputLtd.val(($(this).text()).toLowerCase());
			box.slideUp(300, function(){
				hold.removeClass('open');
			});
			return false;
		});
		
		$(document).bind('click touchstart mousedown', function(e){
			if(!($(e.target).parents().index(hold) != -1 || $(e.target).index(box) != -1 )){
				box.slideUp(300, function(){
					hold.removeClass('open');
				});
			}
		});
	});
}

function moreList(){
	$('.sect').each(function(){
		var hold = $(this);
		var box = hold.find('>.in');
		var btn = hold.find('.btns .btn.toggle');
		
		box.css({display: 'none'});
		btn.on('click', function(){
			$(this).css({display: 'none'});
			hold.addClass('open');
			box.slideDown(300);
		});
	});
}

/**
 * jQuery gallery v2.3.4
 * Copyright (c) 2013 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

;(function($){var _installDirections=function(data){data.holdWidth=data.list.parent().outerWidth();data.woh=data.elements.outerWidth(true);if(!data.direction)data.parentSize=data.holdWidth;else{data.woh=data.elements.outerHeight(true);data.parentSize=data.list.parent().height()}data.wrapHolderW=Math.ceil(data.parentSize/data.woh);if((data.wrapHolderW-1)*data.woh+data.woh/2>data.parentSize)data.wrapHolderW--;if(data.wrapHolderW==0)data.wrapHolderW=1},_dirAnimate=function(data){if(!data.direction)return{left:-(data.woh*
data.active)};else return{top:-(data.woh*data.active)}},_initDisableBtn=function(data){data.prevBtn.removeClass(data.disableBtn);data.nextBtn.removeClass(data.disableBtn);if(data.active==0||data.count+1==data.wrapHolderW-1)data.prevBtn.addClass(data.disableBtn);if(data.active==0&&data.count+1==1||data.count+1<=data.wrapHolderW-1)data.nextBtn.addClass(data.disableBtn);if(data.active==data.rew)data.nextBtn.addClass(data.disableBtn)},_initEvent=function(data,btn,side){btn.bind(data.event+".gallery",
function(){if(data.flag){if(data.infinite)data.flag=false;if(data._t)clearTimeout(data._t);_toPrepare(data,side);if(data.autoRotation)_runTimer(data);if(typeof data.onChange=="function")data.onChange({data:data})}if(data.event=="click")return false})},_initEventSwitcher=function(data){data.switcher.bind(data.event+".gallery",function(){if(data.flag&&!$(this).hasClass(data.activeClass)){if(data.infinite)data.flag=false;data.active=data.switcher.index(jQuery(this))*data.slideElement;if(data.infinite)data.active=
data.switcher.index(jQuery(this))+data.count;if(data._t)clearTimeout(data._t);if(data.disableBtn)_initDisableBtn(data);if(!data.effect)_scrollElement(data);else _fadeElement(data);if(data.autoRotation)_runTimer(data);if(typeof data.onChange=="function")data.onChange({data:data})}if(data.event=="click")return false})},_toPrepare=function(data,side){if(!data.infinite){if(data.active==data.rew&&data.circle&&side)data.active=-data.slideElement;if(data.active==0&&data.circle&&!side)data.active=data.rew+
data.slideElement;for(var i=0;i<data.slideElement;i++)if(side){if(data.active+1<=data.rew)data.active++}else if(data.active-1>=0)data.active--}else{if(data.active>=data.count+data.count&&side)data.active-=data.count;if(data.active<=data.count-1&&!side)data.active+=data.count;data.list.css(_dirAnimate(data));if(side)data.active+=data.slideElement;else data.active-=data.slideElement}if(data.disableBtn)_initDisableBtn(data);if(!data.effect)_scrollElement(data);else _fadeElement(data)},_fadeElement=function(data){data.list.removeClass(data.activeClass).css({zIndex:1});
data.list.eq(data.last).stop().css({zIndex:2,opacity:1});if(data.effect=="transparent")data.list.eq(data.last).animate({opacity:0},{queue:false,duration:data.duration});data.list.eq(data.active).addClass(data.activeClass).css({opacity:0,zIndex:3}).animate({opacity:1},{queue:false,duration:data.duration,complete:function(){jQuery(this).css("opacity","auto")}});if(data.autoHeight)data.list.parent().animate({height:data.list.eq(data.active).outerHeight()},{queue:false,duration:data.duration});if(data.switcher)data.switcher.removeClass(data.activeClass).eq(data.active).addClass(data.activeClass);
data.last=data.active},_scrollElement=function(data){data.elements.removeClass("active").eq(data.active).addClass(data.activeClass);if(!data.infinite)data.list.animate(_dirAnimate(data),{queue:false,duration:data.duration});else{data.list.animate(_dirAnimate(data),data.duration,function(){if(data.active>=data.count+data.count)data.active-=data.count;if(data.active<=data.count-1)data.active+=data.count;data.list.css(_dirAnimate(data));data.flag=true});data.elements.eq(data.active-data.count).addClass(data.activeClass);
data.elements.eq(data.active+data.count).addClass(data.activeClass)}if(data.autoHeight)data.list.parent().animate({height:data.list.children().eq(data.active).outerHeight()},{queue:false,duration:data.duration});if(data.switcher)if(!data.infinite)data.switcher.removeClass(data.activeClass).eq(Math.ceil(data.active/data.slideElement)).addClass(data.activeClass);else{data.switcher.removeClass(data.activeClass).eq(data.active-data.count).addClass(data.activeClass);data.switcher.removeClass(data.activeClass).eq(data.active-
data.count-data.count).addClass(data.activeClass);data.switcher.eq(data.active).addClass(data.activeClass)}},_runTimer=function(data){if(data._t)clearTimeout(data._t);data._t=setInterval(function(){if(data.infinite)data.flag=false;_toPrepare(data,true);if(typeof data.onChange=="function")data.onChange({data:data})},data.autoRotation)},_rePosition=function(data){if(data.flexible&&!data.direction){_installDirections(data);if(data.oneSlide)data.elements.css({width:data.holdWidth});else if(data.elements.length*
data.minWidth>data.holdWidth){data.elements.css({width:Math.floor(data.holdWidth/Math.floor(data.holdWidth/data.minWidth))});if(data.elements.outerWidth(true)>Math.floor(data.holdWidth/Math.floor(data.holdWidth/data.minWidth)))data.elements.css({width:Math.floor(data.holdWidth/Math.floor(data.holdWidth/data.minWidth))-(data.elements.outerWidth(true)-Math.floor(data.holdWidth/Math.floor(data.holdWidth/data.minWidth)))})}else{data.active=0;data.elements.css({width:Math.floor(data.holdWidth/data.elements.length)})}}_installDirections(data);
if(!data.effect){data.rew=data.count-data.wrapHolderW+1;if(data.active>data.rew&&!data.infinite)data.active=data.rew;if(data.active-data.count>data.rew&&data.infinite)data.active=data.rew;data.list.css({position:"relative"}).css(_dirAnimate(data));if(data.autoHeight)data.list.parent().css({height:data.list.children().eq(data.active).outerHeight()})}else{data.rew=data.count;data.list.css({opacity:0}).removeClass(data.activeClass).eq(data.active).addClass(data.activeClass).css({opacity:1}).css("opacity",
"auto");if(data.autoHeight)data.list.parent().css({height:data.list.eq(data.active).outerHeight()})}if(data.switcher)if(!data.infinite)data.switcher.removeClass(data.activeClass).eq(Math.ceil(data.active/data.slideElement)).addClass(data.activeClass);else{data.switcher.removeClass(data.activeClass).eq(data.active-data.count).addClass(data.activeClass);data.switcher.removeClass(data.activeClass).eq(data.active-data.count-data.count).addClass(data.activeClass);data.switcher.eq(data.active).addClass(data.activeClass)}if(data.disableBtn)_initDisableBtn(data);
if(data.rew<=0&&!data.effect)data.list.css({left:0})},_initTouchEvent=function(data){var touchOnGallery=false;var startTouchPos,listPosNow,side,start;var span=data.list.parent().find("span.gallery-touch-holder");if(span.length==0){span=$("<span></span>");span.css({position:"absolute",left:0,top:0,width:9999,height:9999,cursor:"pointer",zIndex:9999,display:"none"}).addClass("gallery-touch-holder");data.list.parent().append(span)}data.list.parent().css({position:"relative"});data.list.bind("mousedown.gallery touchstart.gallery",
function(e){touchOnGallery=true;startTouchPos=e.originalEvent.touches?e.originalEvent.touches[0].pageX:e.pageX;data.list.stop();start=0;listPosNow=data.list.position().left;if(e.type=="mousedown")e.preventDefault()});$(document).bind("mousemove.gallery touchmove.gallery",function(e){if(touchOnGallery&&Math.abs(startTouchPos-(e.originalEvent.touches?e.originalEvent.touches[0].pageX:e.pageX))>10){span.css({display:"block"});start=(e.originalEvent.touches?e.originalEvent.touches[0].pageX:e.pageX)-startTouchPos;
if(!data.effect)data.list.css({left:listPosNow+start});return false}}).bind("mouseup.gallery touchend.gallery",function(e){if(touchOnGallery&&span.is(":visible")){span.css({display:"none"});if(!data.infinite)if(!data.effect)if(data.list.position().left>0){data.active=0;_scrollElement(data)}else if(data.list.position().left<-data.woh*data.rew){data.active=data.rew;_scrollElement(data)}else{data.active=Math.floor(data.list.position().left/-data.woh);if(start<0)data.active+=1;_scrollElement(data)}else{if(start<
0)_toPrepare(data,true);if(start>0)_toPrepare(data,false)}else{if(data.list.position().left>-data.woh*data.count)data.list.css({left:data.list.position().left-data.woh*data.count});if(data.list.position().left<-data.woh*data.count*2)data.list.css({left:data.list.position().left+data.woh*data.count});data.active=Math.floor(data.list.position().left/-data.woh);if(start<0)data.active+=1;_scrollElement(data)}if(data.disableBtn)_initDisableBtn(data);if(typeof data.onChange=="function")data.onChange({data:data});
if(data.autoRotation)_runTimer(data);touchOnGallery=false}else touchOnGallery=false})},methods={init:function(options){return this.each(function(){var $this=$(this);$this.data("gallery",jQuery.extend({},defaults,options));var data=$this.data("gallery");data.aR=data.autoRotation;data.context=$this;data.list=data.context.find(data.elements);data.elements=data.list;if(data.elements.css("position")=="absolute"&&data.autoDetect&&!data.effect)data.effect=true;data.count=data.list.index(data.list.filter(":last"));
if(!data.effect)data.list=data.list.parent();data.switcher=data.context.find(data.switcher);if(data.switcher.length==0)data.switcher=false;if(data.nextBtn)data.nextBtn=data.context.find(data.nextBtn);if(data.prevBtn)data.prevBtn=data.context.find(data.prevBtn);if(data.switcher)data.active=data.switcher.index(data.switcher.filter("."+data.activeClass+":eq(0)"));else data.active=data.elements.index(data.elements.filter("."+data.activeClass+":eq(0)"));if(data.active<0)data.active=0;data.last=data.active;
if(data.oneSlide)data.flexible=true;if(data.flexible&&!data.direction)data.minWidth=data.elements.outerWidth(true);_rePosition(data);if(data.flexible&&!data.direction)$(window).bind("resize.gallery",function(){_rePosition(data)});data.flag=true;if(data.infinite){data.count++;data.active+=data.count;data.list.append(data.elements.clone().addClass("gallery-clone"));data.list.append(data.elements.clone().addClass("gallery-clone"));data.list.css(_dirAnimate(data));data.elements=data.list.children()}if(data.rew<=
0&&!data.effect){data.list.css({left:0});return false}if(data.list.length<=1&&data.effect)return $this;if(data.nextBtn)_initEvent(data,data.nextBtn,true);if(data.prevBtn)_initEvent(data,data.prevBtn,false);if(data.switcher)_initEventSwitcher(data);if(data.autoRotation)_runTimer(data);if(data.touch)_initTouchEvent(data);if(typeof data.onChange=="function")data.onChange({data:data})})},option:function(name,set){if(set)return this.each(function(){var data=$(this).data("gallery");if(data)data[name]=set});
else{var ar=[];this.each(function(){var data=$(this).data("gallery");if(data)ar.push(data[name])});if(ar.length>1)return ar;else return ar[0]}},destroy:function(){return this.each(function(){var $this=$(this),data=$this.data("gallery");if(data){if(data._t)clearTimeout(data._t);data.context.find("*").unbind(".gallery");$(window).unbind(".gallery");$(document).unbind(".gallery");data.elements.removeAttr("style");if(data.infinite)data.elements.filter(".gallery-clone").remove();data.list.removeAttr("style");
$this.removeData("gallery")}})},rePosition:function(){return this.each(function(){var $this=$(this),data=$this.data("gallery");_rePosition(data)})},stop:function(){return this.each(function(){var $this=$(this),data=$this.data("gallery");data.aR=data.autoRotation;data.autoRotation=false;if(data._t)clearTimeout(data._t)})},play:function(time){return this.each(function(){var $this=$(this),data=$this.data("gallery");if(data._t)clearTimeout(data._t);data.autoRotation=time?time:data.aR;if(data.autoRotation)_runTimer(data)})},
next:function(element){return this.each(function(){var $this=$(this),data=$this.data("gallery");if(element!="undefined"&&element>-1){data.active=element;if(data.disableBtn)_initDisableBtn(data);if(!data.effect)_scrollElement(data);else _fadeElement(data)}else if(data.flag){if(data.infinite)data.flag=false;if(data._t)clearTimeout(data._t);_toPrepare(data,true);if(data.autoRotation)_runTimer(data);if(typeof data.onChange=="function")data.onChange({data:data})}})},prev:function(){return this.each(function(){var $this=
$(this),data=$this.data("gallery");if(data.flag){if(data.infinite)data.flag=false;if(data._t)clearTimeout(data._t);_toPrepare(data,false);if(data.autoRotation)_runTimer(data);if(typeof data.onChange=="function")data.onChange({data:data})}})}},defaults={infinite:false,activeClass:"active",duration:300,slideElement:1,autoRotation:false,effect:false,elements:"ul:eq(0) > li",switcher:".switcher > li",disableBtn:false,nextBtn:"a.link-next, a.btn-next, .next",prevBtn:"a.link-prev, a.btn-prev, .prev",circle:true,
direction:false,event:"click",autoHeight:false,flexible:false,oneSlide:false,autoDetect:true,touch:true,onChange:null};$.fn.gallery=function(method){if(methods[method])return methods[method].apply(this,Array.prototype.slice.call(arguments,1));else if(typeof method==="object"||!method)return methods.init.apply(this,arguments);else $.error("Method "+method+" does not exist on jQuery.gallery")}})(jQuery);

/**
 * jQuery tabs min v1.0.0
 * Copyright (c) 2011 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

jQuery.fn.tabs=function(options){return new Tabs(this.get(0),options);};function Tabs(context,options){this.init(context,options);}Tabs.prototype={options:{},init:function(context,options){this.options=jQuery.extend({listOfTabs:'a.tab',active:'active',event:'click'},options||{});this.btn=jQuery(context).find(this.options.listOfTabs);this.last=this.btn.index(this.btn.filter('.'+this.options.active));if(this.last==-1)this.last=0;this.btn.removeClass(this.options.active).eq(this.last).addClass(this.options.active);var _this=this;this.btn.each(function(i){if(_this.last==i)jQuery($(this).attr('href')).show();else jQuery($(this).attr('href')).hide();});this.initEvent(this,this.btn);},initEvent:function($this,el){el.bind(this.options.event,function(){if($this.last!=el.index(jQuery(this)))$this.changeTab(el.index(jQuery(this)));return false;});},changeTab:function(ind){jQuery(this.btn.eq(this.last).attr('href')).hide();jQuery(this.btn.eq(ind).attr('href')).show();this.btn.eq(this.last).removeClass(this.options.active);this.btn.eq(ind).addClass(this.options.active);this.last=ind;}}

/**
 * jQuery simplebox v2.0.4
 * Copyright (c) 2013 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

;(function($){var _condition=function(id,options){if($.simplebox.modal){var data=$.simplebox.modal.data('simplebox');data.onClose($.simplebox.modal);$.simplebox.modal.fadeOut(data.duration,function(){$.simplebox.modal.css({left:'-9999px',top:'-9999px'}).show();data.afterClose($.simplebox.modal);$.simplebox.modal.removeData('simplebox');$.simplebox.modal=false;_toPrepare(id,options);});}else _toPrepare(id,options);},_calcWinWidth=function(){return($(document).width()>$('body').width())?$(document).width():jQuery('body').width();},_toPrepare=function(id,options){$.simplebox.modal=$(id);$.simplebox.modal.data('simplebox',options);var data=$.simplebox.modal.data('simplebox');data.btnClose=$.simplebox.modal.find(data.linkClose);var popupTop=$(window).scrollTop()+($(window).height()/2)-$.simplebox.modal.outerHeight(true)/2;if($(window).scrollTop()>popupTop)popupTop=$(window).scrollTop();if(popupTop+$.simplebox.modal.outerHeight(true)>$(document).height())popupTop=$(document).height()-$.simplebox.modal.outerHeight(true);if(popupTop<0)popupTop=0;if(!data.positionFrom){$.simplebox.modal.css({zIndex:1000,top:popupTop,left:_calcWinWidth()/2-$.simplebox.modal.outerWidth(true)/2}).hide();}else{$.simplebox.modal.css({zIndex:1000,top:$(data.positionFrom).offset().top+$(data.positionFrom).outerHeight(true),left:$(data.positionFrom).offset().left}).hide();}_initAnimate(data);_closeEvent(data,data.btnClose);if(data.overlay.closeClick)_closeEvent(data,$.simplebox.overlay);},_initAnimate=function(data){data.onOpen($.simplebox.modal);if(data.overlay){$.simplebox.overlay.css({background:data.overlay.color,opacity:data.overlay.opacity}).fadeIn(data.duration,function(){$.simplebox.modal.fadeIn(data.duration,function(){$.simplebox.busy=false;data.afterOpen($.simplebox.modal);if($(window).scrollTop()>$.simplebox.modal.offset().top)$('html, body').animate({scrollTop:$.simplebox.modal.offset().top},500);});});}else{$.simplebox.overlay.fadeOut(data.duration);$.simplebox.modal.fadeIn(data.duration,function(){$.simplebox.busy=false;data.afterOpen($.simplebox.modal);if($(window).scrollTop()>$.simplebox.modal.offset().top)$('html, body').animate({scrollTop:$.simplebox.modal.offset().top},500);});}},_closeEvent=function(data,element){element.unbind('click.simplebox').bind('click.simplebox',function(){if(!$.simplebox.busy){$.simplebox.busy=true;data.onClose($.simplebox.modal);$.simplebox.modal.fadeOut(data.duration,function(){$.simplebox.modal.css({left:'-9999px',top:'-9999px'}).show();$.simplebox.overlay.fadeOut(data.duration,function(){data.afterClose($.simplebox.modal);$.simplebox.modal.removeData('simplebox');$.simplebox.modal=false;$.simplebox.busy=false});});}return false;});},methods={init:function(options){$(this).unbind('click.simplebox').bind('click.simplebox',function(){var data=$(this).data('simplebox');if(!$(this).hasClass(defaults.disableClass)&&!$.simplebox.busy){$.simplebox.busy=true;_condition($(this).attr('href')?$(this).attr('href'):$(this).data('href'),jQuery.extend(true,{},defaults,options));}return false;});return this;},option:function(name,set){if(set){return this.each(function(){var data=$(this).data('simplebox');if(data)data[name]=set;});}else{var ar=[];this.each(function(){var data=$(this).data('simplebox');if(data)ar.push(data[name]);});if(ar.length>1)return ar;else return ar[0];}}},defaults={duration:300,linkClose:'.close, .btn-close',disableClass:'disabled',overlay:{box:'simplebox-overlay',color:'black',closeClick:true,opacity:0.69},positionFrom:false,onOpen:function(){},afterOpen:function(){},onClose:function(){},afterClose:function(){}};$.fn.simplebox=function(method){if(methods[method]){return methods[method].apply(this,Array.prototype.slice.call(arguments,1));}else{if(typeof method==='object'||!method){return methods.init.apply(this,arguments);}else{$.error('Method '+method+' does not exist on jQuery.simplebox');}}};$.simplebox=function(id,options){if(!$.simplebox.busy){$.simplebox.busy=true;_condition(id,jQuery.extend(true,{},defaults,options));}};$.simplebox.init=function(){if(!$.simplebox.overlay){$.simplebox.overlay=jQuery('<div class="'+defaults.overlay.box+'"></div>');jQuery('body').append($.simplebox.overlay);$.simplebox.overlay.css({position:'fixed',zIndex:999,left:0,top:0,width:'100%',height:'100%',background:defaults.overlay.color,opacity:defaults.overlay.opacity}).hide();};$(document).unbind('keypress.simplebox').bind('keypress.simplebox',function(e){if($.simplebox.modal&&$.simplebox.modal.is(':visible')&&e.keyCode==false){$.simplebox.close();}});$(window).bind('resize.simplebox',function(){if($.simplebox.modal&&$.simplebox.modal.is(':visible')){var data=$.simplebox.modal.data('simplebox');if(!data.positionFrom){$.simplebox.modal.animate({left:_calcWinWidth()/2-$.simplebox.modal.outerWidth(true)/2},{queue:false,duration:$.simplebox.modal.data('simplebox').duration});}else{$.simplebox.modal.animate({top:$(data.positionFrom).offset().top+$(data.positionFrom).outerHeight(true),left:$(data.positionFrom).offset().left},{queue:false,duration:$.simplebox.modal.data('simplebox').duration});}}});};$.simplebox.close=function(){if($.simplebox.modal&&!$.simplebox.busy){var data=$.simplebox.modal.data('simplebox');$.simplebox.busy=true;data.onClose($.simplebox.modal);$.simplebox.modal.fadeOut(data.duration,function(){$.simplebox.modal.css({left:'-9999px',top:'-9999px'}).show();if($.simplebox.overlay)$.simplebox.overlay.fadeOut(data.duration,function(){data.afterClose($.simplebox.modal);$.simplebox.modal.removeData('simplebox');$.simplebox.modal=false;$.simplebox.busy=false;});else{data.afterClose($.simplebox.modal);$.simplebox.modal.removeData('simplebox');$.simplebox.modal=false;$.simplebox.busy=false;}});}};$(document).ready(function(){$.simplebox.init();});})(jQuery);

/**
 * jQuery browser v1.0.0
 * Copyright (c) 2013 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

;(function(e,t,n){"use strict";var r,i;e.uaMatch=function(e){e=e.toLowerCase();var t=/(opr)[\/]([\w.]+)/.exec(e)||/(chrome)[ \/]([\w.]+)/.exec(e)||/(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(e)||/(webkit)[ \/]([\w.]+)/.exec(e)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(e)||/(msie) ([\w.]+)/.exec(e)||e.indexOf("trident")>=0&&/(rv)(?::| )([\w.]+)/.exec(e)||e.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(e)||[];var n=/(ipad)/.exec(e)||/(iphone)/.exec(e)||/(android)/.exec(e)||/(windows phone)/.exec(e)||/(win)/.exec(e)||/(mac)/.exec(e)||/(linux)/.exec(e)||[];var r=/(ipad)/.exec(e)||/(iphone)/.exec(e)||/(android)/.exec(e)||/(windows phone)/.exec(e)||[];return{browser:t[3]||t[1]||"",version:t[2]||"0",platform:n[0]||"",mobile:r}};r=e.uaMatch(t.navigator.userAgent);i={};if(r.browser){i[r.browser]=true;i.version=r.version;i.versionNumber=parseFloat(r.version,10)}if(r.platform){i[r.platform]=true}i.mobile=r.mobile.length?true:false;if(i.chrome||i.opr||i.safari){i.webkit=true}if(i.rv){var s="msie";r.browser=s;i[s]=true}if(i.opr){var o="opera";r.browser=o;i[o]=true}i.name=r.browser;i.platform=r.platform;e.browser=i})(jQuery,window);
