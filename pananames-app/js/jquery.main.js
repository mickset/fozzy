$(document).ready(function(){
initNotice();
jcf.replaceAll();
	clearInputs();
	jQuery('textarea').autoResize({
		animate: false,
		extraSpace: 0
		});
	// $('div.tabs').each(function(){
	// 	$(this).tabs();
	// });
	$('#open-search').bind('change', function(){
		if($(this).is(':checked')){
			$('.search input:text').focus();
		}
		else{
			$('.search input:text').blur();
		}
	});
	$('textarea').trigger('keydown');
	onoff();
});

$(window).load(function () {
	initSwipeTabs();
});

function onoff () {
	$('.onoff').each(function(){
		var hold = $(this);
		var link = hold.find('.line > span');
        var left = link.position().left;
		var w = link.parent().outerWidth();
		var check = hold.find('input:checkbox');
		var touchOnGallery = false,
			startTouchPos,
			start,
			listPosNow;

		
		link.bind("mousedown touchstart", function(e) {
            touchOnGallery = true;
            startTouchPos = e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX;
            start = 0;
            listPosNow = link.position().left;
            if (e.type == "mousedown") e.preventDefault()
        });
        $(document).bind("mousemove touchmove", function(e) {
            if (touchOnGallery && Math.abs(startTouchPos - (e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX)) > 2) {
                start = (e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX) - startTouchPos;
                link.css({
                    left: listPosNow + start < left ? left : (listPosNow + start > w ? w : listPosNow + start)
                });
                return false;
            }
        }).bind("mouseup touchend", function(e) {
            if (touchOnGallery) {
                if(start > 0){
                	link.animate({left: w}, 300, function () {
                		link.removeAttr('style');
                	});
                	check.prop('checked', true);
                }
                if(start < 0){
                	link.animate({left: left}, 300, function () {
                		link.removeAttr('style');
                	});
                	check.prop('checked', false);
                }
                touchOnGallery = false
            } else touchOnGallery = false
        });
        hold.click(function(){
        	
        	if(start < 2 && start > -2){
        		if(check.is(':checked')){
	        		link.animate({left: left}, 300, function () {
	            		link.removeAttr('style');
	            	});
	            	check.prop('checked', false);
	        	}
	        	else{
	        		link.animate({left: w}, 300, function () {
	            		link.removeAttr('style');
	            	});
	            	check.prop('checked', true);
	        	}
        	}
        	return false;
        });
	});
}

function initSwipeTabs () {
	$('.with-tabs').each(function(){
		var hold = $(this);
		var box = hold.find('.tabs');
		var link = box.find('.tab');
		var gall = hold.find('.tabs-content');
		var line = $('<span class="swipe-line" />');
		var w = 100/link.length;

		line.css({
			width: w + '%'
		});

		box.append(line);
		
		hold.gallery({
			duration: 300,
			elements: '.swipe-wrap > .container',
			switcher: '.tabs > .tab',
			circle: false,
			autoHeight: true,
			easing: 'easeOutQuad',
			flexible: false,
			autoDetect: true,
			touch: true,
			onChange: function (o) {
				line.animate({
					left: w*o.data.active + '%'
				}, {queue:false, duration: o.data.duration});
			},
			onTouchMove: function (obj) {
				line.css({
					left: w*this.active + (obj.move/(obj.width/100)/100)*w*-1 + '%'
				});
			}
		});

		$(window).bind('resize', function(){
			hold.gallery('rePosition');
		});
	});
}

function initNotice(){
	var t = $('div.notice > .t');
	var h = t.outerHeight();
	
	t.css({marginTop:-(h/2)});
}

function clearInputs(){
	$('div.input input, div.input textarea').each(function(){
		var _el = $(this);
		var _val = _el.val();
		
		if (_el.parent().hasClass('placeholder')) {
			_el.bind('focus', function(){
				if (this.value == _val) {
					this.value = '';
					$(this).parent().removeClass('placeholder');
				}
				$(this).parent().addClass('focus');
			})
			.bind('blur', function(){
				if(this.value == '') {
					this.value = _val;
					$(this).parent().addClass('placeholder');
				}
				$(this).parent().removeClass('focus');
			});
		}
		else {
			_el.bind('focus', function(){
				$(this).parent().addClass('focus');
				})
			.bind('blur', function(){
				$(this).parent().removeClass('focus');
			});
		};
	});
};

/*
 * jQuery autoResize (textarea auto-resizer)
 * @copyright James Padolsey http://james.padolsey.com
 * @version 1.04.1 (kama fix)
 */
(function(b){b.fn.autoResize=function(f){var a=b.extend({onResize:function(){},animate:!0,animateDuration:150,animateCallback:function(){},extraSpace:20,limit:1E3},f);this.filter("textarea").each(function(){var d=b(this).css({"overflow-y":"hidden",display:"block"}),f=d.height(),g=function(){var c={};b.each(["height","width","lineHeight","textDecoration","letterSpacing"],function(b,a){c[a]=d.css(a)});return d.clone().removeAttr("id").removeAttr("name").css({position:"absolute",top:0,left:-9999}).css(c).attr("tabIndex","-1").insertBefore(d)}(),h=null,e=function(){g.height(0).val(b(this).val()).scrollTop(1E4);var c=Math.max(g.scrollTop(),f)+a.extraSpace,e=b(this).add(g);h!==c&&(h=c,c>=a.limit?b(this).css("overflow-y",""):(a.onResize.call(this),a.animate&&"block"===d.css("display")?e.stop().animate({height:c},a.animateDuration,a.animateCallback):e.height(c)))};d.unbind(".dynSiz").bind("keyup.dynSiz",e).bind("keydown.dynSiz",e).bind("change.dynSiz",e)});return this}})(jQuery);

/**
 * jQuery gallery v2.3.0
 * Copyright (c) 2013 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

;
(function($) {
    var _installDirections = function(data) {
            data.holdWidth = data.list.parent().outerWidth();
            data.woh = data.elements.outerWidth(true);
            if (!data.direction) data.parentSize = data.holdWidth;
            else {
                data.woh = data.elements.outerHeight(true);
                data.parentSize = data.list.parent().height()
            }
            data.wrapHolderW = Math.ceil(data.parentSize / data.woh);
            if ((data.wrapHolderW - 1) * data.woh + data.woh / 2 > data.parentSize) data.wrapHolderW--;
            if (data.wrapHolderW == 0) data.wrapHolderW = 1
        },
        _dirAnimate = function(data) {
            if (!data.direction) return {
                left: -(data.woh * data.active)
            };
            else return {
                top: -(data.woh * data.active)
            }
        },
        _initDisableBtn = function(data) {
            data.prevBtn.removeClass(data.disableBtn);
            data.nextBtn.removeClass(data.disableBtn);
            if (data.active == 0 || data.count + 1 == data.wrapHolderW - 1) data.prevBtn.addClass(data.disableBtn);
            if (data.active == 0 && data.count + 1 == 1 || data.count + 1 <= data.wrapHolderW - 1) data.nextBtn.addClass(data.disableBtn);
            if (data.active == data.rew) data.nextBtn.addClass(data.disableBtn)
        },
        _initEvent = function(data, btn, side) {
            btn.bind(data.event + ".gallery",
                function() {
                    if (data.flag) {
                        if (data.infinite) data.flag = false;
                        if (data._t) clearTimeout(data._t);
                        _toPrepare(data, side);
                        if (data.autoRotation) _runTimer(data);
                        if (typeof data.onChange == "function") data.onChange({
                            data: data
                        });
                    }
                    if (data.event == "click") return false
                })
        },
        _initEventSwitcher = function(data) {
            data.switcher.bind(data.event + ".gallery", function() {
                if (data.flag && !$(this).hasClass(data.activeClass)) {
                    if (data.infinite) data.flag = false;
                    data.active = data.switcher.index(jQuery(this)) * data.slideElement;
                    if (data.infinite) data.active = data.switcher.index(jQuery(this)) + data.count;
                    if (data._t) clearTimeout(data._t);
                    if (data.disableBtn) _initDisableBtn(data);
                    if (!data.effect) _scrollElement(data);
                    else _fadeElement(data);
                    if (data.autoRotation) _runTimer(data);
                    if (typeof data.onChange == "function") data.onChange({
                        data: data
                    });
                }
                if (data.event == "click") return false
            })
        },
        _toPrepare = function(data, side) {
            if (!data.infinite) {
                if (data.active == data.rew && data.circle && side) data.active = -data.slideElement;
                if (data.active == 0 && data.circle && !side) data.active = data.rew + data.slideElement;
                for (var i = 0; i < data.slideElement; i++)
                    if (side) {
                        if (data.active + 1 <= data.rew) data.active++
                    } else if (data.active - 1 >= 0) data.active--
            } else {
                if (data.active >= data.count + data.count && side) data.active -= data.count;
                if (data.active <= data.count - 1 && !side) data.active += data.count;
                data.list.css(_dirAnimate(data));
                if (side) data.active += data.slideElement;
                else data.active -= data.slideElement
            }
            if (data.disableBtn) _initDisableBtn(data);
            if (!data.effect) _scrollElement(data);
            else _fadeElement(data)
        },
        _fadeElement = function(data) {
            data.list.removeClass(data.activeClass).css({
                zIndex: 1
            });
            data.list.eq(data.last).stop().css({
                zIndex: 2,
                opacity: 1
            });
            data.list.eq(data.active).addClass(data.activeClass).css({
                opacity: 0,
                zIndex: 3
            }).animate({
                opacity: 1
            }, {
                queue: false,
                duration: data.duration,
                complete: function() {
                    jQuery(this).css("opacity", "auto")
                }
            });
            if (data.autoHeight) data.list.parent().animate({
                height: data.list.eq(data.active).outerHeight()
            }, {
                queue: false,
                duration: data.duration
            });
            if (data.switcher) data.switcher.removeClass(data.activeClass).eq(data.active).addClass(data.activeClass);
            data.last = data.active
        },
        _scrollElement = function(data) {
        	data.elements.removeClass('active').eq(data.active).addClass(data.activeClass);
            if (!data.infinite){
            	data.list.animate(_dirAnimate(data), {
	                queue: false,
	                easing: data.easing,
	                duration: data.duration
	            });
            }
            else {
            	data.list.animate(_dirAnimate(data), data.duration, data.easing, function() {
	                if (data.active >= data.count + data.count) data.active -= data.count;
	                if (data.active <= data.count - 1) data.active += data.count;
	                data.list.css(_dirAnimate(data));
	                data.flag = true
	            });
	            data.elements.eq(data.active - data.count).addClass(data.activeClass);
	            data.elements.eq(data.active + data.count).addClass(data.activeClass);
            }
            if (data.autoHeight) data.list.parent().animate({
                height: data.list.children().eq(data.active).outerHeight()
            }, {
                queue: false,
                duration: data.duration
            });
            if (data.switcher)
                if (!data.infinite) data.switcher.removeClass(data.activeClass).eq(Math.ceil(data.active / data.slideElement)).addClass(data.activeClass);
                else {
                    data.switcher.removeClass(data.activeClass).eq(data.active - data.count).addClass(data.activeClass);
                    data.switcher.removeClass(data.activeClass).eq(data.active - data.count - data.count).addClass(data.activeClass);
                    data.switcher.eq(data.active).addClass(data.activeClass)
                }
        },
        _runTimer = function(data) {
            if (data._t) clearTimeout(data._t);
            data._t = setInterval(function() {
                if (data.infinite) data.flag = false;
                _toPrepare(data, true);
                if (typeof data.onChange == "function") data.onChange({
                    data: data
                })
            }, data.autoRotation)
        },
        _rePosition = function(data) {
            if (data.flexible && !data.direction) {
                _installDirections(data);
                if(data.oneSlide){
                	data.elements.css({
                        width: data.holdWidth
                    });
                }
                else{
                	if (data.elements.length * data.minWidth > data.holdWidth) {
	                    data.elements.css({
	                        width: Math.floor(data.holdWidth / Math.floor(data.holdWidth / data.minWidth))
	                    });
	                    if (data.elements.outerWidth(true) > Math.floor(data.holdWidth / Math.floor(data.holdWidth / data.minWidth))) data.elements.css({
	                        width: Math.floor(data.holdWidth / Math.floor(data.holdWidth / data.minWidth)) - (data.elements.outerWidth(true) - Math.floor(data.holdWidth / Math.floor(data.holdWidth / data.minWidth)))
	                    })
	                } else {
	                    data.active = 0;
	                    data.elements.css({
	                        width: Math.floor(data.holdWidth / data.elements.length)
	                    })
	                }
                }
            }
            _installDirections(data);
            if (!data.effect) {
                data.rew = data.count - data.wrapHolderW + 1;
                if (data.active > data.rew && !data.infinite) data.active = data.rew;
                if (data.active - data.count > data.rew && data.infinite) data.active = data.rew;
                data.list.css({
                    position: "relative"
                }).css(_dirAnimate(data));
                if (data.autoHeight) data.list.parent().css({
                    height: data.list.children().eq(data.active).outerHeight()
                })
            } else {
                data.rew = data.count;
                data.list.css({
                    opacity: 0
                }).removeClass(data.activeClass).eq(data.active).addClass(data.activeClass).css({
                    opacity: 1
                }).css("opacity", "auto");
                if (data.autoHeight) data.list.parent().css({
                    height: data.list.eq(data.active).outerHeight()
                })
            }
            if (data.switcher)
                if (!data.infinite) data.switcher.removeClass(data.activeClass).eq(Math.ceil(data.active / data.slideElement)).addClass(data.activeClass);
                else {
                    data.switcher.removeClass(data.activeClass).eq(data.active - data.count).addClass(data.activeClass);
                    data.switcher.removeClass(data.activeClass).eq(data.active - data.count - data.count).addClass(data.activeClass);
                    data.switcher.eq(data.active).addClass(data.activeClass)
                }
            if (data.disableBtn) _initDisableBtn(data)
        },
        _initTouchEvent = function(data) {
            var span = $("<span></span>");
            var touchOnGallery = false;
            var startTouchPos, listPosNow, side, start;
            span.css({
                position: "absolute",
                left: 0,
                top: 0,
                width: 9999,
                height: 9999,
                cursor: "pointer",
                zIndex: 9999,
                display: "none"
            });
            data.list.parent().css({
                position: "relative"
            }).append(span);
            data.list.bind("mousedown.gallery touchstart.gallery", function(e) {
                touchOnGallery = true;
                startTouchPos = e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX;
                data.list.stop();
                start = 0;
                listPosNow =
                    data.list.position().left;
                if (e.type == "mousedown") e.preventDefault()
            });
            $(document).bind("mousemove.gallery touchmove.gallery", function(e) {
                if (touchOnGallery && Math.abs(startTouchPos - (e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX)) > 10) {
                    span.css({
                        display: "block"
                    });
                    start = (e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX) - startTouchPos;
                    if (!data.effect) data.list.css({
                        left: listPosNow + start
                    });
                   	if (typeof data.onTouchMove == "function") data.onTouchMove({
                        move: start,
                        width: data.woh
                    });
                    return false;
                }
            }).bind("mouseup.gallery touchend.gallery", function(e) {
                if (touchOnGallery &&
                    span.is(":visible")) {
                    span.css({
                        display: "none"
                    });
                    if (!data.infinite)
                        if (!data.effect)
                            if (data.list.position().left > 0) {
                                data.active = 0;
                                _scrollElement(data)
                            } else if (data.list.position().left < -data.woh * data.rew) {
                        data.active = data.rew;
                        _scrollElement(data)
                    } else {
                        data.active = Math.floor(data.list.position().left / -data.woh);
                        if (start < 0) data.active += 1;
                        _scrollElement(data)
                    } else {
                        if (start < 0) _toPrepare(data, true);
                        if (start > 0) _toPrepare(data, false)
                    } else {
                        if (data.list.position().left > -data.woh * data.count) data.list.css({
                            left: data.list.position().left -
                                data.woh * data.count
                        });
                        if (data.list.position().left < -data.woh * data.count * 2) data.list.css({
                            left: data.list.position().left + data.woh * data.count
                        });
                        data.active = Math.floor(data.list.position().left / -data.woh);
                        if (start < 0) data.active += 1;
                        _scrollElement(data)
                    }
                    if (data.disableBtn) _initDisableBtn(data);
                    if (typeof data.onChange == "function") data.onChange({
                        data: data
                    });
                    if (data.autoRotation) _runTimer(data);
                    touchOnGallery = false
                } else touchOnGallery = false
            })
        },
        methods = {
            init: function(options) {
                return this.each(function() {
                    var $this = $(this);
                    $this.data("gallery", jQuery.extend({}, defaults, options));
                    var data = $this.data("gallery");
                    data.aR = data.autoRotation;
                    data.context = $this;
                    data.list = data.context.find(data.elements);
                    data.elements = data.list;
                    if (data.elements.css("position") == "absolute" && data.autoDetect) data.effect = true;
                    data.count = data.list.index(data.list.filter(":last"));
                    if (!data.effect) data.list = data.list.parent();
                    data.switcher = data.context.find(data.switcher);
                    if (data.switcher.length == 0) data.switcher = false;
                    if (data.nextBtn) data.nextBtn = data.context.find(data.nextBtn);
                    if (data.prevBtn) data.prevBtn = data.context.find(data.prevBtn);
                    if (data.switcher) data.active = data.switcher.index(data.switcher.filter("." + data.activeClass + ":eq(0)"));
                    else data.active = data.elements.index(data.elements.filter("." + data.activeClass + ":eq(0)"));
                    if (data.active < 0) data.active = 0;
                    data.last = data.active;
                    if(data.oneSlide) data.flexible = true;
                    if (data.flexible && !data.direction) data.minWidth = data.elements.outerWidth(true);
                    _rePosition(data);
                    if (data.flexible && !data.direction) $(window).bind("resize.gallery", function() {
                        _rePosition(data)
                    });
                    data.flag = true;
                    if (data.infinite) {
                        data.count++;
                        data.active += data.count;
                        data.list.append(data.elements.clone());
                        data.list.append(data.elements.clone());
                        data.list.css(_dirAnimate(data));
                        data.elements = data.list.children()
                    }
                    if (data.rew < 0 && !data.effect) {
                        data.list.css({
                            left: 0
                        });
                        return false
                    }
                    if (data.list.length <= 1 && data.effect) return $this;
                    if (data.nextBtn) _initEvent(data, data.nextBtn, true);
                    if (data.prevBtn) _initEvent(data, data.prevBtn, false);
                    if (data.switcher) _initEventSwitcher(data);
                    if (data.autoRotation) _runTimer(data);
                    if (data.touch) _initTouchEvent(data);
                    if (typeof data.onChange == "function") data.onChange({
                        data: data
                    });
                });
            },
            option: function(name, set) {
                if (set) return this.each(function() {
                    var data = $(this).data("gallery");
                    if (data) data[name] = set
                });
                else {
                    var ar = [];
                    this.each(function() {
                        var data = $(this).data("gallery");
                        if (data) ar.push(data[name])
                    });
                    if (ar.length > 1) return ar;
                    else return ar[0]
                }
            },
            destroy: function() {
                return this.each(function() {
                    var $this = $(this),
                        data = $this.data("gallery");
                    if (data) {
                        if (data._t) clearTimeout(data._t);
                        data.context.find("*").unbind(".gallery");
                        $(window).unbind(".gallery");
                        $(document).unbind(".gallery");
                        data.elements.removeAttr("style");
                        data.list.removeAttr("style");
                        $this.removeData("gallery")
                    }
                })
            },
            rePosition: function() {
                return this.each(function() {
                    var $this = $(this),
                        data = $this.data("gallery");
                    _rePosition(data)
                })
            },
            stop: function() {
                return this.each(function() {
                    var $this = $(this),
                        data = $this.data("gallery");
                    data.aR = data.autoRotation;
                    data.autoRotation = false;
                    if (data._t) clearTimeout(data._t)
                })
            },
            play: function(time) {
                return this.each(function() {
                    var $this =
                        $(this),
                        data = $this.data("gallery");
                    if (data._t) clearTimeout(data._t);
                    data.autoRotation = time ? time : data.aR;
                    if (data.autoRotation) _runTimer(data)
                })
            },
            next: function(element) {
                return this.each(function() {
                    var $this = $(this),
                        data = $this.data("gallery");
                    if (element != "undefined" && element > -1) {
                        data.active = element;
                        if (data.disableBtn) _initDisableBtn(data);
                        if (!data.effect) _scrollElement(data);
                        else _fadeElement(data)
                    } else if (data.flag) {
                        if (data.infinite) data.flag = false;
                        if (data._t) clearTimeout(data._t);
                        _toPrepare(data, true);
                        if (data.autoRotation) _runTimer(data);
                        if (typeof data.onChange == "function") data.onChange({
                            data: data
                        })
                    }
                })
            },
            prev: function() {
                return this.each(function() {
                    var $this = $(this),
                        data = $this.data("gallery");
                    if (data.flag) {
                        if (data.infinite) data.flag = false;
                        if (data._t) clearTimeout(data._t);
                        _toPrepare(data, false);
                        if (data.autoRotation) _runTimer(data);
                        if (typeof data.onChange == "function") data.onChange({
                            data: data
                        });
                    }
                })
            }
        },
        defaults = {
            infinite: false,
            activeClass: "active",
            duration: 300,
            slideElement: 1,
            autoRotation: false,
            effect: false,
            elements: "ul:eq(0) > li",
            switcher: ".switcher > li",
            disableBtn: false,
            nextBtn: "a.link-next, a.btn-next, .next",
            prevBtn: "a.link-prev, a.btn-prev, .prev",
            circle: true,
            direction: false,
            event: "click",
            autoHeight: false,
            easing: "easeOutQuad",
            flexible: false,
            oneSlide: false,
            autoDetect: true,
            touch: true,
            onChange: null,
            onTouchMove: null
        };
    $.fn.gallery = function(method) {
        if (methods[method]) return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        else if (typeof method === "object" ||
            !method) return methods.init.apply(this, arguments);
        else $.error("Method " + method + " does not exist on jQuery.gallery")
    };
    jQuery.easing["jswing"] = jQuery.easing["swing"];
    jQuery.extend(jQuery.easing, {
        def: "easeOutQuad",
        swing: function(x, t, b, c, d) {
            return jQuery.easing[jQuery.easing.def](x, t, b, c, d)
        },
        easeOutQuad: function(x, t, b, c, d) {
            return -c * (t /= d) * (t - 2) + b
        },
        easeOutCirc: function(x, t, b, c, d) {
            return c * Math.sqrt(1 - (t = t / d - 1) * t) + b
        }
    })
})(jQuery);

/**
 * jQuery tabs min v1.0.0
 * Copyright (c) 2011 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

jQuery.fn.tabs=function(options){return new Tabs(this.get(0),options);};function Tabs(context,options){this.init(context,options);}Tabs.prototype={options:{},init:function(context,options){this.options=jQuery.extend({listOfTabs:'a.tab',active:'active',event:'click'},options||{});this.btn=jQuery(context).find(this.options.listOfTabs);this.last=this.btn.index(this.btn.filter('.'+this.options.active));if(this.last==-1)this.last=0;this.btn.removeClass(this.options.active).eq(this.last).addClass(this.options.active);var _this=this;this.btn.each(function(i){if(_this.last==i)jQuery($(this).attr('href')).show();else jQuery($(this).attr('href')).hide();});this.initEvent(this,this.btn);},initEvent:function($this,el){el.bind(this.options.event,function(){if($this.last!=el.index(jQuery(this)))$this.changeTab(el.index(jQuery(this)));return false;});},changeTab:function(ind){jQuery(this.btn.eq(this.last).attr('href')).hide();jQuery(this.btn.eq(ind).attr('href')).show();this.btn.eq(this.last).removeClass(this.options.active);this.btn.eq(ind).addClass(this.options.active);this.last=ind;}}
