$(document).ready(function(){
	if (navigator.userAgent.toLowerCase().indexOf('windows') != -1) $('body').addClass('windows');
    $('.menu > ul > li > .grad1').gradientText({
        colors: ['#ff7d00', '#ff8300']
    });
    $('.menu > ul > li > .grad2').gradientText({
        colors: ['#ff8a00', '#ff9900']
    });
    $('.menu > ul > li > .grad3').gradientText({
        colors: ['#ffa100', '#ffae00']
    });
    $('.menu > ul > li > .grad4').gradientText({
        colors: ['#ffb600', '#ffc100']
    });
    $('.menu > ul > li > .grad5').gradientText({
        colors: ['#69b440', '#53bd82']
    });
    $('.menu > ul > li > .grad6').gradientText({
        colors: ['#49c1a0', '#40c5ba']
    });
    $('.menu2 .color1 > ul > li > a').gradientText({
        colors: ['#65ad3e', '#40c5ba']
    });
    $('.menu2 .color2 > ul > li > a').gradientText({
        colors: ['#bf9142', '#e3bc7e']
    });
    $('.menu2 .color3 > ul > li > a').gradientText({
        colors: ['#b1535a', '#8c6c9c']
    });
	
	clearInputs();
	clearInputs2();
	initMoveBox();
	jQuery('textarea').autoResize({
		animate: false,
		extraSpace: 0
	});
	$('textarea').trigger('keydown');
	initAllCheckbox();
	$('ul.accounts-tabs').each(function(){
		$(this).tabs();
	});
});

function initAllCheckbox () {
	$('.accounts-block .accounts-check-all').parent().each(function(){
		var hold = $(this);
		var one = hold.find('.accounts-check-all input:checkbox');
		var all = hold.find('.accounts-tld-list input:checkbox');
		
		one.change(function () {
			if(one.is(':checked')){
				all.prop('checked', true);
			}
			else{
				all.prop('checked', false);
			}
		});
	});
}

function initMoveBox () {
	$('.accounts-action-menu2').each(function(){
		var hold = $(this);
		var link = hold.find('.acc-toggle');
		var box = hold.find('.accounts-modal');
		var close = hold.find('.acc-close, .accounts-btn.bg-green');
		var overlay = hold.find('.acc-fader');
		
		link.click(function(){
			if(!hold.hasClass('open')){
				hold.addClass('open');
				overlay.fadeIn(300);
				box.fadeIn(300);
			}
			else{
				overlay.fadeOut(300);
				box.fadeOut(300, function(){
					hold.removeClass('open');
				});
			}
			return false;
		});
		close.add(overlay).click(function(){
			overlay.fadeOut(300);
			box.fadeOut(300, function(){
				hold.removeClass('open');
			});
			return false;
		});
	});
	$('.accounts-block').each(function(){
		var hold = $(this);
		var checkDesTab = hold.find('td input:checkbox');
		var checkMob = hold.find('.show-mob .accounts-domains input:checkbox');
		var box = $('.accounts-action-menu');
		var boxDes = box;
		var boxTab = $('.accounts-action-menu2.for-tab');
		var boxMob = $('.accounts-action-menu2.for-mob');
		var _scroll = function () {
			if(hold.offset().top > $(window).scrollTop() + 30){
				box.css({
					top: hold.offset().top - ($(window).scrollTop() + 30) + 30
				});
			}
			else{
				if(hold.offset().top + hold.outerHeight() < $(window).scrollTop() + 30 + box.outerHeight()){
					box.css({
						top: 30 - (($(window).scrollTop() + 30 + box.outerHeight()) - (hold.offset().top + hold.outerHeight()))
					});
				}
				else{
					box.css({
						top: 30
					});
				}
			}
		};
		var _change = function () {
			if(checkDesTab.filter(':checked').length > 0){
				boxDes.fadeIn(300);
				boxTab.show();
			}
			else{
				boxDes.fadeOut(300);
				boxTab.hide();
			}
			if(checkMob.filter(':checked').length > 0){
				boxMob.show();
			}
			else{
				boxMob.hide();
			}
			_scroll();
		};
		_change();
		checkDesTab.add(checkMob).bind('change', _change);
		$(window).bind('scroll resize', _change);
	});
}

$(document).ready(function(){
	$(".link-scroll").on("click", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
		top = $(id).offset().top;
		$('body,html').animate({scrollTop: top}, 500);
	});
});

function clearInputs(){
	$('div.input2 input, div.input2 textarea').each(function(){
		var _el = $(this);
		var _val = _el.val();
		
		if (_el.parent().hasClass('placeholder')) {
			_el.bind('focus', function(){
				if (this.value == _val) {
					this.value = '';
					$(this).parent().removeClass('placeholder');
				}
				$(this).parent().addClass('focus');
			})
			.bind('blur', function(){
				if(this.value == '') {
					this.value = _val;
					$(this).parent().addClass('placeholder');
				}
				$(this).parent().removeClass('focus');
			});
		}
		else {
			_el.bind('focus', function(){
				$(this).parent().addClass('focus');
				})
			.bind('blur', function(){
				$(this).parent().removeClass('focus');
			});
		};
	});
};

function clearInputs2(){
	$('input.placeholder, textarea.placeholder').each(function(){
		var _el = $(this);
		var _val = _el.val();
		
		_el.bind('focus', function(){
			if (this.value == _val) {
				this.value = '';
				$(this).removeClass('placeholder');
			}
			$(this).addClass('input-focus');
		}).bind('blur', function(){
			if(this.value == '') {
				this.value = _val;
				$(this).addClass('placeholder');
			}
			$(this).removeClass('input-focus');
		});
	});
}


/*
 * jQuery autoResize (textarea auto-resizer)
 * @copyright James Padolsey http://james.padolsey.com
 * @version 1.04.1 (kama fix)
 */
(function(b){b.fn.autoResize=function(f){var a=b.extend({onResize:function(){},animate:!0,animateDuration:150,animateCallback:function(){},extraSpace:20,limit:1E3},f);this.filter("textarea").each(function(){var d=b(this).css({"overflow-y":"hidden",display:"block"}),f=d.height(),g=function(){var c={};b.each(["height","width","lineHeight","textDecoration","letterSpacing"],function(b,a){c[a]=d.css(a)});return d.clone().removeAttr("id").removeAttr("name").css({position:"absolute",top:0,left:-9999}).css(c).attr("tabIndex","-1").insertBefore(d)}(),h=null,e=function(){g.height(0).val(b(this).val()).scrollTop(1E4);var c=Math.max(g.scrollTop(),f)+a.extraSpace,e=b(this).add(g);h!==c&&(h=c,c>=a.limit?b(this).css("overflow-y",""):(a.onResize.call(this),a.animate&&"block"===d.css("display")?e.stop().animate({height:c},a.animateDuration,a.animateCallback):e.height(c)))};d.unbind(".dynSiz").bind("keyup.dynSiz",e).bind("keydown.dynSiz",e).bind("change.dynSiz",e)});return this}})(jQuery);

/**
 * jQuery tabs min v1.0.0
 * Copyright (c) 2011 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

jQuery.fn.tabs=function(options){return new Tabs(this.get(0),options);};function Tabs(context,options){this.init(context,options);}Tabs.prototype={options:{},init:function(context,options){this.options=jQuery.extend({listOfTabs:'a.tab',active:'active',event:'click'},options||{});this.btn=jQuery(context).find(this.options.listOfTabs);this.last=this.btn.index(this.btn.filter('.'+this.options.active));if(this.last==-1)this.last=0;this.btn.removeClass(this.options.active).eq(this.last).addClass(this.options.active);var _this=this;this.btn.each(function(i){if(_this.last==i)jQuery($(this).attr('href')).show();else jQuery($(this).attr('href')).hide();});this.initEvent(this,this.btn);},initEvent:function($this,el){el.bind(this.options.event,function(){if($this.last!=el.index(jQuery(this)))$this.changeTab(el.index(jQuery(this)));return false;});},changeTab:function(ind){jQuery(this.btn.eq(this.last).attr('href')).hide();jQuery(this.btn.eq(ind).attr('href')).show();this.btn.eq(this.last).removeClass(this.options.active);this.btn.eq(ind).addClass(this.options.active);this.last=ind;}}
